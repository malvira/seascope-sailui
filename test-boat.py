#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

from PyQt4 import QtGui
from PyQt4 import QtCore

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from boat import *

class SailRC(QtGui.QWidget):
  
    def __init__(self):
        super(SailRC, self).__init__()
        
        self.initUI()
        
    def initUI(self):

        vbox = QtGui.QVBoxLayout()
        hbox = QtGui.QHBoxLayout()
        
        rudder = QtGui.QSlider(QtCore.Qt.Horizontal, self)
        rudder.setFocusPolicy(QtCore.Qt.NoFocus)
        rudder.setRange(-90,90)

        sailvbox = QVBoxLayout()
        sail = QtGui.QSlider(QtCore.Qt.Vertical, self)
        sail.setFocusPolicy(QtCore.Qt.NoFocus)
        sail.setRange(0,90)

        closeHauled = QSpinBox()
        closeHauled.setMaximum(90)
        closeHauled.setMinimum(0)
        closeHauled.setFocusPolicy(QtCore.Qt.NoFocus)
        
        sailvbox.addWidget(sail)
        sailvbox.addWidget(closeHauled)

        main = QtGui.QSlider(QtCore.Qt.Vertical, self)
        main.setFocusPolicy(QtCore.Qt.NoFocus)
        main.setRange(-90,90)

        jib = QtGui.QSlider(QtCore.Qt.Vertical, self)
        jib.setFocusPolicy(QtCore.Qt.NoFocus)
        jib.setRange(-90,90)

        jibCommand = QtGui.QSlider(QtCore.Qt.Vertical, self)
        jibCommand.setFocusPolicy(QtCore.Qt.NoFocus)
        jibCommand.setRange(-90,90)

        windd = QtGui.QSlider(QtCore.Qt.Vertical, self)
        windd.setFocusPolicy(QtCore.Qt.NoFocus)
        windd.setRange(0,360)

        winds = QtGui.QSlider(QtCore.Qt.Vertical, self)
        winds.setFocusPolicy(QtCore.Qt.NoFocus)
        winds.setRange(0,30)

        heading = QtGui.QSlider(QtCore.Qt.Vertical, self)
        heading.setFocusPolicy(QtCore.Qt.NoFocus)
        heading.setRange(0,360)

        headingCommand = QtGui.QSlider(QtCore.Qt.Vertical, self)
        headingCommand.setFocusPolicy(QtCore.Qt.NoFocus)
        headingCommand.setRange(0,360)

        boat = Boat()
        rudder.valueChanged.connect(boat.setRudder)
        jib.valueChanged.connect(boat.setJib)
        jibCommand.valueChanged.connect(boat.setJibCommand)
        windd.valueChanged.connect(boat.setWindD)
        winds.valueChanged.connect(boat.setWindSpeed)
        sail.valueChanged.connect(boat.setMainCommand)
        main.valueChanged.connect(boat.setMain)
        closeHauled.valueChanged.connect(boat.setCloseHauled)
        heading.valueChanged.connect(boat.setHeading)
        headingCommand.valueChanged.connect(boat.setHeadingCommand)

        hbox.addWidget(headingCommand)
        hbox.addWidget(heading)
        hbox.addWidget(winds)
        hbox.addWidget(windd)
        hbox.addWidget(jib)
        hbox.addWidget(jibCommand)
        hbox.addWidget(main)
        hbox.addLayout(sailvbox)
        hbox.addWidget(boat)
        hbox.setStretchFactor(sailvbox,1)
        hbox.setStretchFactor(boat,100)

        vbox.addLayout(hbox)
        vbox.addWidget(rudder)

        self.setLayout(vbox)

        # self.connect(rudder, QtCore.SIGNAL('valueChanged(int)'), 
        #              self.rudderchange)
        # self.connect(sail, QtCore.SIGNAL('valueChanged(int)'), 
        #              self.sailchange)
                
        self.setWindowTitle('Sail RC')

    # def rudderchange(self, value):
    #     print "Rudder: " + str(value)

    # def sailchange(self, value):
    #     print "Sail: " + str(value)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Left:
            print "left"
        if event.key() == QtCore.Qt.Key_Right:
            print "right"
        if event.key() == QtCore.Qt.Key_Up:
            print "up"
        if event.key() == QtCore.Qt.Key_Down:
            print "down"

if __name__ == '__main__':
  
    app = QtGui.QApplication(sys.argv)
    rc = SailRC()
    rc.show()
    app.exec_()
