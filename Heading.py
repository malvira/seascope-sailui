import math

def offset_heading(heading, offset):
    """ offset a heading to the left and right by offset """
    #left offset
    if heading - offset < 0:
        left = 360 - (offset - heading)
    else:
        left = heading - offset
    
    if heading + offset > 360:
        right = offset - (360 - heading)
    else:
        right = heading + offset
        
    return (left, right)

def subtract_headings(a, b):
    """ compute a - b and return the left distance and the right distance """
    #left error
    if(b > a):
        l = a + (360 - b)
    else:
        l = a - b

    #right error
    if(b < a):
        r = 360 - a + b
    else:
        r = b - a

    return (l, r)

def add_heading_right(a, b):
    """ return a + b to the right of a """
    if(a+b>360):
        return a+b-360
    else:
        return a+b

def add_heading_left(a, b):
    """ return a + b to the right of a """
    if(a-b < 0):
        return 360 - (b - a)
    else:
        return a - b

def heading_within_headings(x, a, b):
    """ return true if x is between the small zone between a and b """
    (l,r) = subtract_headings(a,b)
    small_zone = min(l,r)
    (lxa, rxa) = subtract_headings(a,x)
    (lxb, rxb) = subtract_headings(b,x)
    if min(lxa, rxa) < small_zone and min(lxb, rxb) < small_zone:
        return True
    else: 
        return False
