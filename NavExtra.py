import sys
import logging

from LatLon import *
from Heading import *
from Annotation import *

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *


class WaypointItem(QGraphicsItem):
    def __init__(self, loc, seqno, radius=10):
        super(WaypointItem, self).__init__()
        self.loc = loc
        self.seqno = seqno
        self.radius = radius
    def boundingRect(self):
        return QRectF(0,0,10,10)
    def paint(self, qp, opt, widg):
        qp.setBrush(QBrush(QColor("white")))
        qp.drawRect(-5,-8,10,16)
        qp.drawText(-3,4,QString(str(self.seqno)))

def OffsetReachBuoys(locA, locB, locC, dist):
    new_ways = []

    center = OffsetFromLineCenter(locA, locB, locC, dist)

    center_lat = center.lat
    center_lon = center.lon

    lineslope = LineSlope(locA, locB)
    lineangle = LineAngle(locA, locB)
    length_cm = loc_distance_cm(locA, locB)

    dx = length_cm * .25

    dlat = cm_to_lat(dx*sin(lineangle))
    dlon = cm_to_lon(center_lon, dx*cos(lineangle))

    way_A = Loc(center_lat + dlat,
                center_lon + dlon)
    way_B = Loc(center_lat - dlat,
                center_lon - dlon)

    new_ways.append(way_A)
    new_ways.append(way_B)

    return new_ways

def OffsetFromLineCenter(locA, locB, locC, dist):
    # make a point perpendicularly offset from center of line A, B by
    # distance dist that's on the opposite side of the line from locC.

    dx = dist * cos(LineAngle(locA, locB) - pi/2)
    dy = dist * sin(LineAngle(locA, locB) - pi/2)
    
    center_lat = (locA.lat+locB.lat)/2
    center_lon = (locA.lon+locB.lon)/2

    loc1 = Loc(center_lat + cm_to_lat(dy),
               center_lon + cm_to_lon(center_lat,dx))
    loc2 = Loc(center_lat - cm_to_lat(dy),
               center_lon - cm_to_lon(center_lat,dx))

    if SameSideLine(locA, locB, locC, loc1):
        return loc2
    else:
        return loc1

def SameSideLine(locA, locB, locC, locD):
    # return 1 if locC and locB are on the same side of line locA --> locB
    Ax = locA.lon
    Bx = locB.lon
    Cx = locC.lon
    Dx = locD.lon
    Ay = locA.lat
    By = locB.lat
    Cy = locC.lat
    Dy = locD.lat

    return (cmp( (Bx - Ax) * (Cy - Ay) - (By -Ay) * (Cx - Ax) , 0) ==
            cmp( (Bx - Ax) * (Dy - Ay) - (By -Ay) * (Dx - Ax) , 0))
def DiffSideLine(locA, locB, locC, locD):
    # return 1 if locC and locB are on the same side of line locA --> locB
    Ax = locA.lon
    Bx = locB.lon
    Cx = locC.lon
    Dx = locD.lon
    Ay = locA.lat
    By = locB.lat
    Cy = locC.lat
    Dy = locD.lat

    return (cmp( (Bx - Ax) * (Cy - Ay) - (By -Ay) * (Cx - Ax) , 0) !=
            cmp( (Bx - Ax) * (Dy - Ay) - (By -Ay) * (Dx - Ax) , 0))

def LineSlope(locA, locB):
    # return slope of line given by endpoints A and B
    Ax = locA.lon
    Bx = locB.lon
    Ay = locA.lat
    By = locB.lat


    dy = lat_to_cm(By) - lat_to_cm(Ay)
    dx = lon_to_cm(Ay,Bx) - lon_to_cm(Ay,Ax)


    if dx == 0 :
        return 10000
    else:
        return dy/dx

def LineAngle(locA, locB):
    # return angle of line given by endpoints A and B
    Ax = locA.lon
    Bx = locB.lon
    Ay = locA.lat
    By = locB.lat


    return atan2(Ay - By,Ax - Bx)


def BetweenBuoys(locA,locB,locC):
    # true if locC is between buoys A and B

    # BUG: doesn't work if one of the points is at (0,0)

    Ax = locA.lon
    Bx = locB.lon
    Cx = locC.lon
    Ay = locA.lat
    By = locB.lat
    Cy = locC.lat

    return ( ( (Cx > Ax and Cx < Bx) or (Cx < Ax and Cx > Bx) ) and
             ( (Cy > Ay and Cy < By) or (Cy < Ay and Cy > By) ))


def ProjToLine(locA, locB, locC):
# projects point C onto line formed by A and B.
# doesn't guarantee that new point is between A and B
    Ax = locA.lon
    Bx = locB.lon
    Cx = locC.lon
    Ay = locA.lat
    By = locB.lat
    Cy = locC.lat

    line_dx = Bx - Ax
    line_dy = By - Ay

    line_slope = LineSlope(locA, locB)
    if line_slope == 0:
        proj_slope = 10000
    else:
        proj_slope = -1 / line_slope

    # find intersection point
    # t1 = ((y1-y2)*dx2 - (x1-x2)*dy2) / (dx1*dy2-dy1*dx2)

    if (line_dx*proj_slope - line_dy*1) == 0:
        t1 = 0
    else:
        t1 = (((locA.lat - Cy)*1 - (locA.lon -
                                    Cx) * proj_slope) / 
              (line_dx*proj_slope - line_dy*1))
            
    return Loc(locA.lat + t1 * line_dy,
               locA.lon + t1 * line_dx)

def ReachFromTo(locA, locB):
# reach from loc A to loc B

    locs = []


    line_angle = LineAngle(locB, locA)
    reachlength = loc_distance_cm(locB, locA)

    spacing = 600 

    numwaypoints = ceil(reachlength/spacing)

    dlat = cm_to_lat(spacing*sin(line_angle))
    dlon = cm_to_lon(locA.lat, spacing * cos(line_angle))

    for iway in range(int(numwaypoints - 1)):
        locs.append(
            Loc(locA.lat + (iway+1)*dlat,
                locA.lon + (iway+1)*dlon)
            )
    return locs
