import sys
import logging
import re
logger = logging.getLogger(__name__)
import socket

from boat import BoatItem

def recToDict(record):
    pairs = record.split(';')
    d = {}
    for pair in pairs:
        try:
            (key,value) = pair.split(':')
        except ValueError:
            continue
        d[key] = value
    return d

class WorldServer():
    def __init__(self, server='localhost', port=6006):
        self.server = server
        self.port = port
        self.records = []
    def sendBoatItem(self, boatitem):
        if boatitem.mode == 'simulated':
            logger.info("Simulated boat --- not posting to worldserver")
            return
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((self.server,self.port))
        except:
            logger.warn("can't connect to world server")
            return
        data = "4\nTYPE:SailBoat;ID:seascope;Lat:" + str(boatitem.loc.lat) + ";Lon:" + str(boatitem.loc.lon) + ";Hdg:" + str(boatitem.boat.heading) + ";Spd:0;Radius:50;Team:SeaScope;WDir:" + str(boatitem.boat.windDirection) + ";WSpd:0;\n\n"
        s.send(data)
        s.close()
    def getRecords(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((self.server,self.port))
        except:
            logger.warn("can't connect to world server")
            return
        data = "2\n"
        try:
            s.send(data)
        except:
            s.close()
            logger.warn("can't connect to world server")
            logger.warn("can't connect to world server")
            return
        msg = ""
        while not msg.endswith("\n\n"):
            try:
                char = s.recv(1)
            except:
                s.close()
                logger.warn("can't connect to world server")
                return
            msg = msg + char
        s.close()
        records = msg.split('\n')
        logger.debug(records)
        recs = []
        for idx, rec in enumerate(records):
            if idx == 0 : continue
            if idx >= len(records) - 2: continue
            recs.append(recToDict(rec))
        return recs
