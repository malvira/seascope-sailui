import sys
import logging
logger = logging.getLogger(__name__)

from LatLon import *
from Heading import *
from Annotation import *
from NavExtra import *

from navigatorRounder import *
from navigatorFleetRace import *
from navigatorNavigation import *
from navigatorObstacle import *

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class navigatorWaypoints():
    def __init__(self, water, buoys):
        self.water = water
        self.buoys = buoys
        self.loc = Loc(0,0)
        logger.debug("create waypoint navigator")
        ### TODO keep a list of all waypoints 
        self.updateWaypoints()
    def updateWaypoints(self):
        """ recompute waypoint positions """
        self.water.clearWaypoints()
        key = self.buoys.keys()[0]
        self.loc = self.buoys[key].loc
        self.water.addWaypoint(WaypointItem(self.loc, 1))
    def currentWaypoint(self):
        self.water.clearAnnotations()

        key = self.buoys.keys()[0]
        self.loc = self.buoys[key].loc
        logger.debug("Waypoints current waypoint: " + str(self.loc.lat) + " " + str(self.loc.lon))
        return self.loc


