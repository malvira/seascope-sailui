import sys
import logging

class DisplayObj():
    def __init__(self, time=0):
        self.time = time
        self.packets = 0
        self.waypoint = -1
        self.lat = 0
        self.lon = 0
        self.rudder = 0
        self.boom = 0
        self.winddir = 0
        self.water = -1
        self.starttimecount = -1
        self.timecount = -1

disp = DisplayObj()

def printDisplay(obj):
        print("\x1B[2J"); #clear screen
        print("\x1B[H");  #cursor home                                                                      
        print("time     " + str(obj.time))        
        print("packets  " + str(obj.packets))
        print("---------")
        print("waypoint       " + str(obj.waypoint))
        print("starttimecount " + str(obj.starttimecount))
        print("timecount      " + str(obj.timecount))
        print("---------")
        print("lat      " + str(obj.lat) + " lon " + str(obj.lon))
        print("rudder   " + str(obj.rudder))
        print("boom     " + str(obj.boom))
        print("winddir  " + str(obj.winddir))
        print("water    " + str(obj.waterlevel))
        if(obj.waterlevel > 2000):
            print "!!!DANGER WATER LEVEL DANGER!!!"
