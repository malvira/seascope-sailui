import sys
import logging
logger = logging.getLogger(__name__)

from math import *
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from LatLon import *
from Map import *
from boat import *
import Nav

from Obstacle import Obstacle

class AncCirc(QGraphicsEllipseItem):
    def __init__(self):
        pass

class WaterScene(QGraphicsScene):
    clicked = pyqtSignal(QPointF)

    def __init__(self, a, b, c, d):
        super(WaterScene, self).__init__(a, b, c, d)
    def mousePressEvent(self, event):
        super(WaterScene, self).mousePressEvent(event)
        logging.debug('mouse press ' + str(event.scenePos().x()) + ' ' + str(event.scenePos().y()))
        self.clicked.emit(event.scenePos())
    
class Water(QWidget):
    clicked = pyqtSignal(object)
    buoySelected = pyqtSignal(object)
    
    def __init__(self, parent = None):
        QWidget.__init__(self, parent)
        self.view = QGraphicsView()
        # this is the water grid in centimeters
        self.grid = 5000;
        # number of major boxes on x
        self.major_boxes_x = 10;
        # and y
        self.major_boxes_y = 10;
        # size of a pixel in centimeters
        self.pixel_size_x = (float)(self.major_boxes_x * self.grid) / self.width()
        self.pixel_size_y = (float)(self.major_boxes_y * self.grid) / self.width()
        # center of grid lat and lon
        self.setCenter(Loc(0,0))
        # list of all known maps
        self.maps = [ Map("./maps/charles-lagoon.png",
            Anchor(34, 439, Loc(42.353802, -71.084512)),
            Anchor(1220, 335, Loc(42.354008, -71.081331))),
        Map("./maps/walden.png",
            Anchor(276, 165, Loc(42.502596,-71.011784)),
            Anchor(989, 441, Loc(42.493863,-70.981143))),
        Map("./maps/lubeck.png",
            Anchor(14, 36, Loc(53.87492374860172, 10.695898532867432)),
            Anchor(989, 441, Loc(53.87129293507659, 10.707957744598389))),
        Map("./maps/mystic.png",
            Anchor(20, 24, Loc(42.44185944039708, -71.15604400634766)),
            Anchor(512, 684, Loc(42.43140749679098, -71.14548683166504))),
        Map("./maps/candlewood.png",
            Anchor(69, 270, Loc(41.5480782155943, -73.44860315322876)),
            Anchor(635, 622, Loc(41.545251785700245, -73.44253063201904))),
        Map("./maps/spy.png",
            Anchor(95, 113, Loc(42.41163905704312, -71.16209506988525)),
            Anchor(743, 573, Loc(42.404351040882126, -71.14819049835205))),
        Map("./maps/redwire.png",
            Anchor(204, 194, Loc(42.39902708526245, -71.06845378875732)),
            Anchor(582, 614, Loc(42.39237150546451, -71.06034278869629))),
                      ]
        self.buoys = [];
        self.waypoints = [];
        self.annotations = [];
        self.obstacles = [];

        logger.debug("W: " + str(self.width()) + " H: " + str(self.height()))
        logger.debug("pix x: " + str(self.pixel_size_x) + " pix y: " + str(self.pixel_size_y))

        scene = WaterScene(0,0,self.width(),self.height())
        self.scene = scene
        scene.clicked.connect(self.emitclicked)

        self.box = QGraphicsRectItem(0,0,self.width(),self.height())
        self.scene.addItem(self.box)

        self.center_circ = QtGui.QGraphicsEllipseItem(
            self.scene.width()/2-5, 
            self.scene.height()/2-5, 
            10, 10
            )
        self.scene.addItem(self.center_circ)

        self.boat = BoatItem()
        self.scene.addItem(self.boat)
        x,y = self.loc_to_xy(self.center)
        self.boat.setPos(x,y)

        self.windDir = 0
        self.noGo = NoGoItem(self.boat.noGo)
        self.scene.addItem(self.noGo)

        self.view.setScene(self.scene)

#       self.item.setPos(self.scene.width(),self.scene.height())
        logger.debug("Scene:")
        logger.debug(self.scene.width())
        logger.debug(self.scene.height())

        layout = QHBoxLayout()
        layout.addWidget(self.view, 1)
        self.setLayout(layout)

    def setWindDir(self, windDir):
        self.windDir = windDir
        logger.debug("set windDir: " + str(windDir))

    def setBoatLoc(self, loc):
        dx, dy = self.loc_to_xy(loc)
        self.boat.loc = loc
        self.boat.setPos(dx, dy)
        self.noGo.setPos(dx, dy)
        self.noGo.setRotation(self.windDir)
        self.noGo.setNoGo(self.boat.noGo)
        self.scene.update()

    def emitclicked(self, point):
        loc = self.xy_to_loc(point.x(), point.y())
        logger.debug("water clicked " + str(loc.lat) + ' ' + str(loc.lon))
        self.clicked.emit(loc)

    def setCenter(self, loc):
        self.center = loc
        logger.info("setCenter: " + str(loc.lat) + " " + str(loc.lon))
        try:
            self.update()
        except:
            pass

    def resizeEvent(self, event):
        self.size = event.size()
#        self.scene.setSceneRect(0,0,self.size.width()-30,self.size.height()-30)
        self.update()

    def findNearestMap(self):
        logger.debug("findNearestMap")
        dis = None
        retmap = None
        for m in self.maps:
            logger.debug("   " + m.mapfile)
            this_dis = loc_distance_cm(m.anc1.loc, self.center)
            logger.debug("   " + str(this_dis))
            if ((dis == None) or
                (this_dis < dis)):
                dis = this_dis
                retmap = m
                logger.debug("   retmap " + m.mapfile + " " + str(dis))
        logger.debug("   retmap " + retmap.mapfile + " " + str(dis))
        return retmap

    def update(self):        
        try:
            logger.debug("W: " + str(self.size.width()) + " H: " + str(self.size.height()))
            self.pixel_size_x = (float)(self.major_boxes_x * self.grid) / self.size.width()
            self.pixel_size_y = (float)(self.major_boxes_y * self.grid) / self.size.width()
            logger.debug("pix x: " + str(self.pixel_size_x) + " pix y: " + str(self.pixel_size_y))
        except:
            pass
        
        self.map = self.findNearestMap()
        self.setMap(self.map)
        self.drawAnc1Circ()
        self.setBoatLoc(self.boat.loc)
        for buoy in self.buoys:
            self.itemSetPosFromLoc(buoy, buoy.loc)
        for waypoint in self.waypoints:
            self.itemSetPosFromLoc(waypoint, waypoint.loc)

    def locCirc(self, loc):
        """ make a circ at location loc """
        dx, dy = self.loc_to_xy(loc)
        
        circ = QtGui.QGraphicsEllipseItem(
            dx-5, dy-5,
            10, 10
            ) 
        return circ

    def drawAnc1Circ(self):
        """ redraw anc1 circl """
        try:
            self.scene.removeItem(self.anc1_circ)
        except:
            pass
        self.anc1_circ = self.locCirc(self.map.anc1.loc)

        self.scene.addItem(self.anc1_circ)

    def addWaypoint(self, waypoint):
        logger.info("add waypoint " + str(waypoint.seqno))
        self.waypoints.append(waypoint)
        self.scene.addItem(waypoint)
        waypoint.setZValue(2)
        dx, dy = self.loc_to_xy(waypoint.loc)
        waypoint.setPos(dx, dy)

    def clearWaypoints(self):
        for waypoint in self.waypoints:
            self.scene.removeItem(waypoint)
        self.waypoints = []
        self.update()

    def clearAnnotations(self):
        for ant in self.annotations:
            self.scene.removeItem(ant)
        self.annotations = []
        self.update()

    def clearObstacles(self):
        for obs in self.obstacles:
            self.scene.removeItem(obs)
        self.obstacles = []
        self.update()

    def addObstacle(self, obs):
        self.obstacles.append(obs)
        self.scene.addItem(obs)
        self.itemSetPosFromLoc(obs, obs.loc)
        return obs

    def addBuoy(self, buoy):
        logger.info("add buoy " + str(buoy.type) + " " + str(buoy.seqno))
        buoy.locChanged.connect(self.setBuoyPos)
        buoy.clicked.connect(self.selectBuoy)
        buoy.setLoc(buoy.loc)
        self.buoys.append(buoy)
        self.scene.addItem(buoy.item)
        return buoy

    def selectBuoy(self, buoy):
        logger.info("buoy " + buoy.txt + " selected")
        self.buoySelected.emit(buoy)

    def setBuoyPos(self, buoy):
        logger.warn("set bouy pos")
        logger.warn("lat " + str(buoy.loc.lat) + " " + "lon " + str(buoy.loc.lon))
        self.itemSetPosFromLoc(buoy, buoy.loc)

    def itemSetPosFromLoc(self, item, loc):
        """ use and item's loc element to set it's position """
#        logger.debug("set item pos from loc")
        dx, dy = self.loc_to_xy(loc)
        item.setPos(dx, dy)

    def setMap(self, _map):
        #scale the map
        #compute w and h between the anchors in centimeters
        h = abs(lat_to_cm(_map.anc1.loc.lat) - lat_to_cm(_map.anc2.loc.lat))
        logger.debug("setMap")
        logger.debug(h)
        w = abs(lon_to_cm(self.center.lat, _map.anc1.loc.lon) - lon_to_cm(self.center.lat, _map.anc2.loc.lon))
        logger.debug(w)
        logger.debug(sqrt(w*w + h*h))
        #compute w and h between the anchors in pixles
        pix_h = abs(_map.anc1.y - _map.anc2.y)
        pix_w = abs(_map.anc1.x - _map.anc2.x)
        logger.debug(str(pix_h) + " " + str(pix_w))
        #convert image cm to unscaled number of pixles
        h = h/self.pixel_size_y
        w = w/self.pixel_size_x
        logger.debug(str(h) + " " + str(w))
        scale_x = w/pix_w
        scale_y = h/pix_h

        try:
            self.scene.removeItem(self.mapItem)
        except:
            pass
        self.mapItem = QtGui.QGraphicsPixmapItem(QPixmap(_map.mapfile))
        #set the offset to anc1 so that scaling works on this point
        self.mapItem.setOffset(-_map.anc1.x,-_map.anc1.y)
        self.mapItem.setZValue(-1)
        self.scene.addItem(self.mapItem)

        #translate anchor1 to the center
        self.mapItem.translate(self.scene.width()/2,
                               self.scene.height()/2
                               )
        #translate anchor1 to the real position
        
        logger.debug(lon_to_cm(self.center.lat, _map.anc1.loc.lon - self.center.lon)/self.pixel_size_x)
        logger.debug(lat_to_cm(_map.anc1.loc.lat - self.center.lat)/self.pixel_size_y )

        dx, dy = self.loc_to_xy(_map.anc1.loc)
        dx -= self.scene.width()/2
        dy -= self.scene.height()/2

        self.mapItem.translate(
            dx, dy
            )

        self.mapItem.setTransform(QTransform.fromScale(scale_x, scale_y), 1);
        logger.debug(" ")
 
    def setGrid(self, grid):
        """ sets the size of the grid in centimeters """
        logger.info("zoom " + str(grid))
        self.grid = grid
        self.update()

    def loc_to_xy(self, loc):
        """ return water xy from lat/lon based on current location and grid """
        x = lon_to_cm(self.center.lat, loc.lon - self.center.lon)/self.pixel_size_x
        y = lat_to_cm(-loc.lat + self.center.lat)/self.pixel_size_y
        x = x + self.scene.width()/2
        y = y + self.scene.height()/2

#        logger.debug("loc_to_xy " + str(x) + " " + str(y))
        return x,y

    def xy_to_loc(self, x, y):
        """ return Loc from water xy """
        logger.debug("xy_to_loc " + str(x) + " " + str(y))
        x -= self.scene.width()/2
        y -= self.scene.height()/2
        x *= self.pixel_size_x
        y *= self.pixel_size_y
        return Loc(self.center.lat - cm_to_lat(y), self.center.lon + cm_to_lon(self.center.lat - cm_to_lat(y),x))

    def cm_to_pix(self, cm):
        return int(cm/self.pixel_size_x)

class NoGoItem(QGraphicsItem):
    def __init__(self, noGo):
        self.noGo = noGo
        super(NoGoItem, self).__init__()
    def boundingRect(self):
        return QRectF(0,0,100,100)
    def paint(self, qp, opt, widg):
        qp.drawLine(0,0,0,-50)
        qp.drawLine(0,0,50*sin(self.noGo*2*pi/360),-50*cos(self.noGo*2*pi/360))
        qp.drawLine(0,0,-50*sin(self.noGo*2*pi/360),-50*cos(self.noGo*2*pi/360))
    def setNoGo(self, noGo):
        self.noGo = noGo
#        self.paint()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    logging.basicConfig(level=logging.DEBUG)

    foo = QWidget();

    water = Water()
    zoom = QSlider(QtCore.Qt.Vertical, foo)
    zoom.valueChanged.connect(water.setGrid)
    zoom.setRange(1000,10000)
    zoom.setValue(5000)

    hbox = QHBoxLayout()
    hbox.addWidget(water)
    hbox.addWidget(zoom)

    foo.setLayout(hbox)
    foo.show()

    sys.exit(app.exec_())
