import sys
import logging
import math
logger = logging.getLogger(__name__)

import socket
import time
import os

from PyQt4 import QtGui
from PyQt4 import QtCore

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from LatLon import *
from Heading import *

from Display import disp

try:
    import coapy.connection
    import coapy.options
    import coapy.link
except ImportError:
    coapy = None
    logger.warn("coapy not installed")
    logger.warn("git clone git://coapy.git.sourceforge.net/gitroot/coapy/coapy")
    logger.warn("cd coapy; python setup.py install")

def processCompass(data):
    logger.debug(data)
    (ax,ay,az,mx,my,mz) = data
    ax = float(ax)/16383
    ay = float(ay)/16383
    az = float(az)/16383
    pitch = asin(-ax)
    roll = asin(ay/cos(pitch))
    logger.debug("pitch " + str(pitch) + " roll " + str(roll))
    logger.debug("pitch " + str((pitch/(2*pi))*360) + " roll " + str((roll/(2*pi))*360))


    mx = float(mx)/1055
    my = float(my)/1055
    mz = float(mz)/950

    if mx > 0 and my >= 0:
        heading = (atan(my/mx)/(2*pi))*360
    elif mx < 0:
        heading = 180 + (atan(my/mx)/(2*pi))*360
    elif mx > 0 and my <= 0:
        heading = 360 + (atan(my/mx)/(2*pi))*360
    elif mx == 0 and my <= 0:
        heading = 90
    elif mx == 0 and my > 0:
        heading = 270

    logger.debug("raw heading: " + str(heading))

    mx2 =  mx * cos(pitch)                              + mz * sin(pitch)
    my2 =  mx * sin(roll) * sin(pitch) + my * cos(roll) - mz * sin(roll) * cos(pitch)
    mz2 = -mx * cos(roll) * sin(pitch) + my * sin(roll) + mz * cos(roll) * cos(pitch)

    if mx2 > 0 and my2 >= 0:
        heading = (atan(my2/mx2)/(2*pi))*360
    elif mx2 < 0:
        heading = 180 + (atan(my2/mx2)/(2*pi))*360
    elif mx2 > 0 and my2 <= 0:
        heading = 360 + (atan(my2/mx2)/(2*pi))*360
    elif mx2 == 0 and my2 <= 0:
        heading = 90
    elif mx2 == 0 and my2 > 0:
        heading = 270

    logger.debug("tilt comp heading: " + str(heading))

    (a, b) = offset_heading(heading, 14.5)
    return b

class BoatItem(QGraphicsPolygonItem):
    def __init__(self, parent=None, scene=None):
        super(BoatItem, self).__init__(
            QPolygonF(
                [
                    QPointF(0,-10),
                    QPointF(5,5), 
                    QPointF(-5,5),
                 ]
                )
            )
        logger.debug("create boat item: ")
        self.loc = Loc(53.87247, 10.69876) # set default boat location on luebeck map
        self.host = 'aaaa::250:c2a8:ca00:2'
        self.port = 61616
        self.address_family = socket.AF_INET6        
        self.led = 0
        self.lat = '0N'
        self.lon = '0E'
        self.mode = 'simulated'
        self.noGo = 40
        self.packets = 0
        self.waterlevel = -1

        self.boat = Boat()

        lognum = 1
        while(os.path.exists('log-' + str(lognum) + '.csv')):
            lognum = lognum+1
        self.log = open('log-' + str(lognum) + '.csv', 'w')

    def setRudder(self, angle):
        self.boat.rudderAngle = angle

    def setPos(self, x, y):
        super(BoatItem, self).setPos(x, y)

    def toggleLED(self):
        if self.mode != 'simulated':
            if self.led:
                self.led = 0
                self.post_command('led',0)
            else:
                self.led = 1
                self.post_command('led',1)

    def post_course(self, value):
        self.post_command('course', value)

    def post_rudder(self, value):
        self.post_command('rudder', value)

    def post_sail(self, value):
        self.post_command('main', value)

    def post_command(self, prop, value):
        uri = "cmd_post?prop=" + prop
        valuestr = "value=" + str(value)
        logger.debug("post command " + uri + " " + str(value))

        try:
            ep = coapy.connection.EndPoint(address_family=self.address_family)
            msg = coapy.connection.Message(code=coapy.PUT, payload=valuestr, uri_path=uri)
            remote = (self.host, self.port, 0, 0)
            ep.send(msg,remote)
            ep.process(250)
        except:
            pass

    def post_all(self):
        props = ['rudder', 'main', 'led']

        for prop in props:
            if prop == 'main':
                self.post_command('main', self.boat.mainCommand)
            if prop == 'rudder':
                self.post_command('rudder', self.boat.rudderAngle)
            if prop == 'course':
                self.post_command('course', self.boat.headingCommand)
                            

    def update(self):
        if self.mode == 'simulated':
            self.updateSim()
        else:
            self.updateCoap()
        self.setRotation(self.boat.heading)

    def setHeading(self, heading):
        if self.mode == 'simulated':
            return
        self.boat.heading = heading

    def updateSim(self):
        logger.debug("updateSim")
        # heading moves in proportion to speed and rudder position

        if self.boat.rudderAngle < 0:
            # negative rudder turns boat to the right
            self.boat.heading = add_heading_right(self.boat.heading, abs(sin(self.boat.rudderAngle*2*pi/360)*self.boat.windSpeed))
        else:
            # positive rudder turns boat to the left
            self.boat.heading = add_heading_left(self.boat.heading, abs(sin(self.boat.rudderAngle*2*pi/360)*self.boat.windSpeed))

        # boat speed is windspeed but in cm/s
        self.loc.lat = cm_to_lat(lat_to_cm(self.loc.lat) + cos(self.boat.heading*2*pi/360)*self.boat.windSpeed)
        self.loc.lon = cm_to_lat(lat_to_cm(self.loc.lon) + sin(self.boat.heading*2*pi/360)*self.boat.windSpeed)

    def updateCoap(self):
#        resources = ['jib', 'rudder', 'wind_dir', 'wind_speed', 'heading', 'main', 'jib_command', 'latitude', 'longitude']
        resources = ['jib', 'rudder', 'main', 'latitude', 'longitude', 'compass', 'wind_dir', 'water']

        try:
            for uri in resources:
                remote = (self.host, self.port, 0, 0)
                req = coapy.connection.Message(code=coapy.GET, uri_path=uri)
                ep = coapy.connection.EndPoint(address_family=self.address_family)
                tx_rec = ep.send(req, remote)
                rv = None
                try:
                    rv = ep.process(250)
                except socket.error, (value,message):
                    logger.warn("Could not open socket: " + message)
                    logger.warn("GET " + self.host + "/" + uri + " failed")

                if coapy.OK and (type(rv) is not 'NoneType'):
                    msg = rv.message
                    self.packets += 1

                    try:
                        if uri == 'latitude' or uri == 'longitude' or uri == 'compass':
                            if uri == 'latitude':
                                self.lat = msg.payload.rstrip()
                                logger.info("Boat lat " + self.lat)
                                self.loc = gps_to_loc(self.lat, self.lon)
                            if uri == 'longitude':
                                self.lon = msg.payload.rstrip()
                                logger.info("Boat lon " + self.lon)
                                self.loc = gps_to_loc(self.lat, self.lon)
                            if uri == 'compass':
                                val = msg.payload.rstrip()
#                                logger.info("Compass data: " + val)
                                heading = processCompass(val.split(','))
#                                logger.info("Compass heading: " + str(heading))
                                self.boat.compass = heading
                        else:
                            value = int(msg.payload)

                        if uri == 'water':
                            logger.debug("Read water: " + msg.payload.rstrip())
                            self.waterlevel = int(msg.payload)
                        if uri == 'jib':
                            logger.debug("Read jib: " + msg.payload.rstrip())
                            self.boat.jibAngle = int(msg.payload)
                        if uri == 'jib_limit':
                            logger.debug("Read jib limit: " + msg.payload.rstrip())
                            self.boat.JibCommand = int(msg.payload)
                        if uri == 'rudder':
                            logger.debug("Read Rudder: " + msg.payload.rstrip())
                            self.boat.rudderAngle = int(msg.payload)
                        if uri == 'wind_dir':
                            logger.debug("Read Wind Dir: " + msg.payload.rstrip())
                            self.boat.windDirection = int(msg.payload)
                        if uri == 'wind_speed':
                            logger.debug("Read Wind Speed: " + msg.payload.rstrip())
                            self.boat.windSpeed = int(msg.payload)
                        if uri == 'heading':
                            logger.debug("Read Heading Sensor: " + msg.payload.rstrip())
                            self.boat.headingSensor = int(msg.payload)
                        if uri == 'main':
                            logger.debug("Read Main: " + msg.payload.rstrip())
                            self.boat.mainAngle = int(msg.payload)
                    except ValueError:
                        logger.debug("bad number returned: GET " + self.host + "/" + uri)
        except:
            pass

        global disp
        disp.time = time.time()
        disp.lat = self.lat
        disp.lon = self.lon
        disp.rudder = self.boat.rudderAngle
        disp.boom = self.boat.mainAngle
        disp.winddir = self.boat.windDirection
        disp.packets = self.packets
        disp.waterlevel = self.waterlevel

        entry = str(time.time()) + ',' + self.lat + ',' + self.lon + ',' + str(self.boat.rudderAngle) + ',' + str(self.boat.jibAngle) + ',' + str(self.boat.jibCommand) + ',' + str(self.boat.mainAngle) + ',' + str(self.boat.mainCommand) + ',' + str(self.boat.windDirection) + ',' + str(self.boat.windSpeed) + ',' + str(self.boat.compass) + ',' + str(self.boat.headingCommand) + '\n' 
        self.log.write(entry)

class Boat:
    def __init__(self):
        self.rudderAngle = 0
        self.jibAngle = 0
        self.jibCommand = 0
        self.mainAngle = 0
        self.mainCommand = 45
        self.closeHauled = 0
        self.windDirection = 0
        self.windSpeed = 30
        self.heading = 0
        self.headingSensor = 0
        self.headingCommand = 0
        self.compass = 0

class BoatWidget(QtGui.QWidget):
    def __init__(self, parent = None):      
        QtGui.QWidget.__init__(self, parent)
        self._pointText = {0: "N", 45: "NE", 90: "E", 135: "SE", 180: "S",
                           225: "SW", 270: "W", 315: "NW"}
        self._margins = 10
        self.rudderAngle = 0
        self.jibAngle = 0
        self.jibCommand = 0
        self.mainAngle = 0
        self.mainCommand = 45
        self.closeHauled = 0
        self.windDirection = 0
        self.windSpeed = 5
        self.heading = 0
        self.headingCommand = 0
    def sizeHint(self):
        return QSize(250, 250)
    def paintEvent(self,event=None):
        qp = QPainter()
        qp.begin(self)
        qp.setRenderHint(QPainter.Antialiasing)
        
        qp.fillRect(event.rect(), self.palette().brush(QPalette.Window))
        qp.translate(self.width()/2, self.height()/2)
        scale = min(self.width()/120.0,
                    self.height()/120.0)
        qp.scale(scale*.5, scale*.5)
        
        startAngle = 90 * 16
        arcLength = 90 * 16

        boatW = 20
        boatH = 100
        boatCx = -boatW/2
        boatCy = -boatH/4

        rect = QtCore.QRect(boatCx, boatCy, boatW, boatH)

#debug draws
#boat rectangle
#        qp.drawRect(rect)

        self.pen = QtGui.QPen(QtCore.Qt.black)        
        self.pen.setCapStyle(QtCore.Qt.RoundCap)
        self.pen.setStyle(QtCore.Qt.SolidLine)
        qp.setPen(self.pen)

#draw boat
        qp.drawArc(rect, startAngle, arcLength)
        qp.drawArc(rect, startAngle, -arcLength)
        qp.drawLine(QPoint(-boatW/2,boatH/4),QPoint(boatW/2,boatH/4))

#main command
        qp.save()
        qp.translate(0, -boatH/16)
        if self.mainCommand < self.closeHauled:
            self.maimCommand = self.closeHauled
        qp.rotate(self.mainCommand)
        qp.drawLine(QPoint(0,0),QPoint(0,40))
        qp.rotate(-2*self.mainCommand)
        qp.drawLine(QPoint(0,0),QPoint(0,40))
        qp.restore()

        self.pen = QtGui.QPen(QtCore.Qt.blue)        
        self.pen.setWidth(2)
        self.pen.setCapStyle(QtCore.Qt.RoundCap)
        self.pen.setStyle(QtCore.Qt.SolidLine)
        qp.setPen(self.pen)
#rudder
        rudderLength = 10        
        qp.save()
        qp.translate(0, boatH/4)
        qp.rotate(self.rudderAngle)
        qp.drawLine(QPoint(0,0),QPoint(0,rudderLength))
        qp.restore()

#jib
        jibLength = 15
        qp.save()
        qp.translate(0, -boatH/4)
        qp.rotate(self.jibAngle)
        qp.drawLine(QPoint(0,0),QPoint(0,jibLength))
        qp.restore()

#jib command
        qp.save()

        self.pen = QtGui.QPen(QtCore.Qt.black)
        self.pen.setCapStyle(QtCore.Qt.RoundCap)
        self.pen.setStyle(QtCore.Qt.SolidLine)
        qp.setPen(self.pen)

        qp.translate(0, -boatH/4)
        qp.rotate(self.jibCommand)
        qp.drawLine(QPoint(0,0),QPoint(0,20))
        qp.rotate(-2*self.jibCommand)
        qp.drawLine(QPoint(0,0),QPoint(0,20))
        qp.restore()

        self.pen = QtGui.QPen(QtCore.Qt.blue)
        self.pen.setWidth(2)
        self.pen.setCapStyle(QtCore.Qt.RoundCap)
        self.pen.setStyle(QtCore.Qt.SolidLine)
        qp.setPen(self.pen)
#main
        mainLength = 25
        qp.save()
        qp.translate(0, -boatH/16)
        qp.rotate(self.mainAngle)
        qp.drawLine(QPoint(0,0),QPoint(0,mainLength))
        qp.restore()

#wind arrow
        self.pen = QtGui.QPen(QtCore.Qt.red)        
        self.pen.setWidth(2)
        self.pen.setCapStyle(QtCore.Qt.RoundCap)
        self.pen.setStyle(QtCore.Qt.SolidLine)
        qp.setBrush(Qt.red)
        qp.setPen(self.pen)

        arrowW = 15
        arrowL = self.windSpeed
        arrowHeadL = 10
        arrowOffset = -75 
        qp.save()
        qp.translate(0,0)
        qp.rotate(self.windDirection)
        qp.drawPolygon(
            QPolygon([QPoint(arrowW/4, arrowOffset), QPoint(arrowW/4, 2*arrowL/4+arrowOffset), QPoint(2*arrowW/4, 2*arrowL/4+arrowOffset),
                      QPoint(0, 2*arrowL/4+arrowOffset+arrowHeadL),
                      QPoint(-2*arrowW/4, 2*arrowL/4+arrowOffset), QPoint(-arrowW/4, 2*arrowL/4+arrowOffset), QPoint(-arrowW/4, arrowOffset)
                      ])
            )
        qp.restore()

#compass markings
        qp.save()
        
        qp.scale(1.8, 1.8)

        font = QFont(self.font())
        font.setPixelSize(10)
        metrics = QFontMetricsF(font)
        
        qp.setFont(font)
        qp.setPen(self.palette().color(QPalette.Shadow))
        
        qp.rotate(self.heading)
        i = 0
        while i  < 360:
        
            if i % 45 == 0:
                qp.drawLine(0, -40, 0, -50)
                qp.drawText(-metrics.width(self._pointText[i])/2.0, -52,
                                 self._pointText[i])
            else:
                qp.drawLine(0, -45, 0, -50)
            
            qp.rotate(15)
            i += 15
        qp.restore()

#heading command

        arrowL = self.headingCommand
        courseOffset = -75 
        courseLength = 40
        qp.save()
        qp.translate(0,0)
        qp.rotate(self.heading - self.headingCommand)
        self.pen = QtGui.QPen(QtCore.Qt.red)        
        self.pen.setWidth(2)
        self.pen.setCapStyle(QtCore.Qt.RoundCap)
        self.pen.setStyle(QtCore.Qt.SolidLine)
        qp.setPen(self.pen)
        qp.drawLine(QPoint(0,courseOffset),QPoint(0,courseOffset-courseLength))
        qp.restore()

#painter end
        qp.end()

    @pyqtSlot(int)
    def setRudder(self, angle):
    
        if angle != self.rudderAngle:
            self.rudderAngle = angle
            self.update()

    @pyqtSlot(int)
    def setJib(self, angle):
    
        if angle != self.jibAngle:
            self.jibAngle = angle
            self.update()

    @pyqtSlot(int)
    def setJibCommand(self, angle):

        if angle != self.jibCommand:
            self.jibCommand = angle
            self.update()

    @pyqtSlot(int)
    def setMain(self, angle):
    
        if angle != self.mainAngle:
            self.mainAngle = angle
            self.update()

    @pyqtSlot(int)
    def setHeading(self, angle):
    
        if angle != self.heading:
            self.heading = angle
            self.update()

    @pyqtSlot(int)
    def setHeadingCommand(self, angle):
    
        if angle != self.heading:
            self.headingCommand = angle
            self.update()

    @pyqtSlot(int)
    def setWindD(self, angle):
    
        if angle != self.windDirection:
            self.windDirection = angle
            self.update()

    @pyqtSlot(int)
    def setWindSpeed(self, speed):
    
        if speed != self.windSpeed:
            self.windSpeed = speed
            self.update()

    @pyqtSlot(int)
    def setMainCommand(self, angle):
    
        if angle != self.mainCommand:
            if angle < self.closeHauled:
                angle = self.closeHauled
            self.mainCommand = angle
            self.update()

    @pyqtSlot(int)
    def setCloseHauled(self, angle):
        self.closeHauled = angle
