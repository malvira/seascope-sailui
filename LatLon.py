#lat/long to feet.
#lat 0.001' = 6.074 feet
#lat 0.0000001 1.11 cm
#http://calgary.rasc.ca/latlong.htm
#The Formula for Longitude Distance at a Given Latitude (theta) in Km:
#1deg of Longitude = 111.41288 * cos(theta) - 0.09350 * cos(3 * theta) + 0.00012 * cos(5 * theta)

import re
from math import *

class Loc:
	def __init__(self, lat, lon):
		self.lat = lat
		self.lon = lon

def loc_distance_cm(a, b):
	"""Computes the distance (in cm) between two Locs"""
	"""uses the average latitude between the two points to compute lon distance"""
	avg_lat = (a.lat + b.lat) / 2
	dis_lat = lat_to_cm(a.lat) - lat_to_cm(b.lat)
	dis_lon = lon_to_cm(avg_lat, a.lon) - lon_to_cm(avg_lat, b.lon)
	return sqrt(dis_lat*dis_lat + dis_lon*dis_lon)

def lat_to_cm(lat):
#lat 0.0000001 1.11 cm
        ret = (1.11/0.0000001) * lat
        return ret

def lon_to_cm(lat, lon):
#1deg of Longitude = 111.41288 * cos(theta) - 0.09350 * cos(3 * theta) + 0.00012 * cos(5 * theta)
        theta = (abs(lat)/180.0) * pi
        cm_per_lon = 100000 * (111.41288 * cos(theta) - 0.09350 * cos(3 * theta) + 0.00012 * cos(5 * theta))
        return lon * cm_per_lon

def cm_to_lat(cm):
#lat 0.0000001 1.11 cm
        ret = (0.0000001/1.11) * cm
        return ret

def cm_to_lon(lat, cm):
#1deg of Longitude = 111.41288 * cos(theta) - 0.09350 * cos(3 * theta) + 0.00012 * cos(5 * theta)
        theta = (abs(lat)/180.0) * pi
        cm_per_lon = 100000 * (111.41288 * cos(theta) - 0.09350 * cos(3 * theta) + 0.00012 * cos(5 * theta))
        return (1/cm_per_lon) * cm

def locs_to_heading(locA, locB):
	""" return a heading (0-360) pointing from A to B """

	ax = lon_to_cm((locA.lat + locB.lat) / 2, locA.lon)
	ay = lat_to_cm(locA.lat)
	bx = lon_to_cm((locA.lat + locB.lat) / 2, locB.lon)
	by = lat_to_cm(locB.lat)
	x = bx - ax
	y = by - ay

	try:
		theta = atan(y/x)
	except ZeroDivisionError:
		theta = 0	
	theta = (theta/(2*pi))*360

	if x > 0 and y > 0:
		heading = 90 - theta
	if x <  0 and y > 0:
		heading = 270 - theta
	if x >  0 and y < 0:
		heading = 90 - theta
	if x <  0 and y < 0:
		heading = 270 - theta
	if x == 0 and y >= 0:
		heading = 0
	if x == 0 and y < 0:
		heading = 180
	if y == 0 and x >= 0:
		heading = 90
	if y == 0 and x < 0:
		heading = 270

	return heading

def gps_to_loc(latstr, lonstr):
	return Loc(gps_convert(latstr),gps_convert(lonstr))

def gps_convert(coord):
	p = re.compile('(\d+)(\d\d\.\d+)(\w)')
	m = p.match(coord)
	try:
		coord = int(m.group(1)) + float(m.group(2)) * (1/60.0)
		if(m.group(3) == 'S' or m.group(3) == 'W'):
			coord *= -1
	except:
		coord = 0
	return coord

def debug_print_dis(one,two,real):
	print "Loc1: " + str(one.lat) + " " + str(one.lon)
	print "Loc2: " + str(two.lat) + " " + str(two.lon)
	print loc_distance_cm(one,two)
	print str(real) + " cm"
	print "err: " + str((loc_distance_cm(one,two) - real) / real * 100) + "%"
	print 


if __name__ == '__main__':
#http://www.csgnetwork.com/gpsdistcalc.html	
	debug_print_dis(Loc(42.502596,-71.011784),
			Loc(42.493863,-70.981143),
			269146.041473)

	debug_print_dis(Loc(42.493863,-71.011784),
			Loc(42.493863,-70.981143),
			251059.846931)

	debug_print_dis(Loc(42.502596,-70.981143),
			Loc(42.493863,-70.981143),
			97043.0146587)

	debug_print_dis(Loc(1,0),
			Loc(0,0),
			11057430)

	debug_print_dis(Loc(0,1),
			Loc(0,0),
			11131950)

	debug_print_dis(Loc(90,1),
			Loc(90,0),
			1)

	print gps_convert("4223.8101N")
	print gps_convert("4223.8101E")
	print gps_convert("4223.8101S")
	print gps_convert("4223.8101W")

	print gps_convert("0N")
	print gps_convert("0E")
	print gps_convert("")

	loc = gps_to_loc("4223.8101N", "4223.8101W")
	print loc.lat
	print loc.lon
