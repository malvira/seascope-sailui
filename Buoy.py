import sys
import logging
logger = logging.getLogger(__name__)

from math import *
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from LatLon import *

class BuoyItem(QGraphicsItem):
    def __init__(self, buoy):
        super(BuoyItem, self).__init__()
        self.buoy = buoy
    def boundingRect(self):
        return QRectF(0,0,20,20)
    def paint(self, qp, opt, widg):
        if self.buoy.type == 'r':
            qp.setBrush(QBrush(QColor("red")))
        if self.buoy.type == 'y':
            qp.setBrush(QBrush(QColor("yellow")))
        if self.buoy.type == 'w':
            qp.setBrush(QBrush(QColor("white")))
        qp.drawEllipse(0,0,20,20)
        qp.drawText(7,14,QString(str(self.buoy.seqno)))
    def mousePressEvent(self, event):
        logging.debug('buoy mouse press ' + str(event.scenePos().x()) + ' ' + str(event.scenePos().y()))
        self.buoy.emitMousePress(self.buoy)

class Buoy(QObject):
    locChanged = pyqtSignal(object)
    clicked = pyqtSignal(object)
    def __init__(self, type='r', seqno='', dir='', loc=Loc(0,0) ):
        super(Buoy, self).__init__()
        logger.debug("create buoy: " + type + " " + str(seqno) + " " + dir)
        self.type = type
        self.seqno = seqno
        self.loc = loc
        self.txt = 'None'
        self.wsid = ''
        self.item = BuoyItem(self)
    def setLoc(self, loc):
        logging.debug("buoy set loc")
        self.loc = loc
        self.locChanged.emit(self)
    def emitMousePress(self, buoy):
        logging.debug("buoy emit mouse press")
        self.clicked.emit(buoy)
    def setPos(self, x, y):
        x -= 10
        y -= 10
        self.item.setPos(x, y)

if __name__ == '__main__':
    from Water import Water

    app = QApplication(sys.argv)

    logging.basicConfig(level=logging.DEBUG)
    waterlog = logging.getLogger('Water')
    waterlog.setLevel(logging.INFO)

    foo = QWidget();

    water = Water()
    zoom = QSlider(QtCore.Qt.Vertical, foo)
    zoom.valueChanged.connect(water.setGrid)
    zoom.setRange(1000,10000)
    zoom.setValue(5000)

    hbox = QHBoxLayout()
    hbox.addWidget(water)
    hbox.addWidget(zoom)

    foo.setLayout(hbox)
    foo.show()

    water.setCenter(Loc(0,0))
    b1 = Buoy(type='r', seqno=1, loc=Loc(0,0))
    water.addBuoy(b1)

    sys.exit(app.exec_())

