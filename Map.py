class Map:
    """ maps have two anchors """
    """ each anchor is a pixle location and a lat/log """
    """ anchor 1 gets fixed on the grid coordinates"""
    """ then image is scaled based on anchor2 sizing"""
    """ a little error in the anchor2 position is possible"""
    def __init__(self, mapfile, anc1, anc2):
        self.mapfile = mapfile
        self.anc1 = anc1
        self.anc2 = anc2        

class Anchor:
    def __init__(self, x, y, loc):
        self.x = x
        self.y = y
        self.loc = loc

