import sys
import traceback
import copy
import logging
import time
logger = logging.getLogger(__name__)

from LatLon import *
from Heading import *
from Annotation import *
from NavExtra import *

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from Display import disp

####################################################################3
        

class navigatorStationSimple():
    def __init__(self, water, buoys):
        self.water = water
        self.buoys = buoys
        self.locs = []
        self.state = 0
        self.state = 0
        self.timer_en = False
        self.box_timer = False
        self.starttimecount = 0
        self.tacktime = 0
        self.timeref = 0
        self.lasttack = 0 
        self.timecount = 0
        self.currentLocIndex = 0
        logger.debug("create Station Holding navigator")
        self.updateWaypoints()
    def updateWaypoints(self):
        # StationSimple
        """ recompute waypoint positions """
        self.water.clearWaypoints()
        self.locs = []

        self.sline_side = True

        if self.state == -1:
            self.water.clearWaypoints() # clears waypoints  
            self.locs = []

            [wayA, wayB] = OffsetReachBuoys(self.buoys['Yellow - Entrance 1'].loc,
                                            self.buoys['Yellow - Entrance 2'].loc,
                                            self.buoys['Red - Exit 2'].loc,
                                            400)
            
            self.locs.append(wayA)
            self.locs.append(wayB)


        #---------------- StationSimple
        elif self.state == 0: 
            # figure out point on starting line that doesn't require us to tack
            
            self.water.clearWaypoints() # clears waypoints  
            self.locs = []

            # find headings for the two close-hauled tacks
            (sbTack, pTack) = offset_heading(self.water.windDir, self.water.boat.noGo)

            logger.debug("Starboard tack heading is: " + str(sbTack))
            # find points of intersection with line defined by starting buoys
            x1 = self.buoys['Yellow - Entrance 1'].loc.lon
            y1 = self.buoys['Yellow - Entrance 1'].loc.lat
            x2 = self.buoys['Yellow - Entrance 2'].loc.lon
            y2 = self.buoys['Yellow - Entrance 2'].loc.lat

            x3 = self.water.boat.loc.lon
            y3 = self.water.boat.loc.lat
            x4 = self.water.boat.loc.lon + cm_to_lon(y3, 100)
            y4 = self.water.boat.loc.lat + cm_to_lat(100*tan(sbTack*pi/180))

            x5 = self.water.boat.loc.lon + cm_to_lon(y3, 100)
            y5 = self.water.boat.loc.lat + cm_to_lat(100*tan(pTack*pi/180))

            if ( (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4) == 0 or
                 (x1-x2)*(y3-y5) - (y1-y2)*(x3-x5) == 0):
                isect_x_sb = x1
                isect_x_p = x1
                isect_y_sb = y1
                isect_y_p = y1
            else:
                isect_x_sb = (( (x1*y2 - y1*x2)*(x3-x4) - (x1-x2)*(x3*y4 - y3*x4))/
                           ( (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4)))
                isect_x_p = (( (x1*y2 - y1*x2)*(x3-x5) - (x1-x2)*(x3*y5 - y3*x5))/
                           ( (x1-x2)*(y3-y5) - (y1-y2)*(x3-x5)))
                isect_y_sb = (( (x1*y2 - y1*x2)*(y3-y4) - (y1-y2)*(x3*y4 - y3*x4))/
                              ( (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4)))
                isect_y_p = (( (x1*y2 - y1*x2)*(y3-y5) - (y1-y2)*(x3*y5 - y3*x5))/
                             ( (x1-x2)*(y3-y5) - (y1-y2)*(x3-x5)))

            loc_sb = Loc(isect_y_sb, isect_x_sb)
            loc_p = Loc(isect_y_p, isect_x_p)


            if ( (isect_x_sb > x1 and isect_x_sb < x2) or
                 (isect_x_sb < x1 and isect_x_sb > x2)):
                # sb tack point is on start line (valid)
                if ( (isect_x_p > x1 and isect_x_p < x2) or
                     (isect_x_p < x1 and isect_x_p > x2)):
                    # port tack point is valid
                    
                    # then choose the closer one
                    # (alternatively, may want to choose the tack we're already on
                    if (loc_distance_cm(loc_sb, self.water.boat.loc) >
                        loc_distance_cm(loc_p, self.water.boat.loc)):
                        self.locs.append(loc_p)
                    else:
                        self.locs.append(loc_sb)
                else:
                    # only the sb tack is valid
                    self.locs.append(loc_sb)
            elif ( (isect_x_p > x1 and isect_x_p < x2) or
                   (isect_x_p < x1 and isect_x_p > x2)):
                # only port tack is valid
                self.locs.append(loc_p)
            else: 
                # I give up. sail to center of start line
                self.locs.append(Loc(
                        (y1 + y2) / 2,
                        (x1 + x2) / 2
                        ))


                
        elif self.state == 1:        #------------------ StationSimple

            self.water.clearWaypoints() # clears waypoints 
            self.locs = []

            # find center  line of box
            center_pt1_x = (self.buoys['Yellow - Entrance 1'].loc.lon +
                            self.buoys['Yellow - Entrance 2'].loc.lon)/2
            center_pt1_y = (self.buoys['Yellow - Entrance 1'].loc.lat +
                            self.buoys['Yellow - Entrance 2'].loc.lat)/2
            center_pt2_x = (self.buoys['Red - Exit 1'].loc.lon +
                            self.buoys['Red - Exit 2'].loc.lon)/2
            center_pt2_y = (self.buoys['Red - Exit 1'].loc.lat +
                            self.buoys['Red - Exit 2'].loc.lat)/2

            # angle and slope of line along forward path of box ("vertical")
            self.vert_line_slope = LineSlope(Loc(center_pt1_y,center_pt1_x),
                                             Loc(center_pt2_y,center_pt2_x))
            self.vert_line_angle = atan2( (center_pt2_y - center_pt1_y), 
                                          (center_pt2_x - center_pt1_x))

            logger.debug("vertical line slope is " + str(self.vert_line_slope));

            # angle and slope of "horizonal" lines
            if self.vert_line_slope == 0:
                self.horiz_line_slope = 10000
            else:
                self.horiz_line_slope = -1 / self.vert_line_slope
            self.horiz_line_angle = self.vert_line_angle - pi/2

            # find dimensions of box
            dim_width = (loc_distance_cm(self.buoys['Yellow - Entrance 1'].loc,
                                     self.buoys['Yellow - Entrance 2'].loc) +
                     (loc_distance_cm(self.buoys['Red - Exit 1'].loc,
                                      self.buoys['Red - Exit 2'].loc)))/2
            dim_length = min((loc_distance_cm(self.buoys['Yellow - Entrance 1'].loc,
                                              self.buoys['Red - Exit 1'].loc) +
                              (loc_distance_cm(self.buoys['Yellow - Entrance 2'].loc,
                                               self.buoys['Red - Exit 2'].loc))),
                             (loc_distance_cm(self.buoys['Yellow - Entrance 1'].loc,
                                              self.buoys['Red - Exit 2'].loc) +
                              (loc_distance_cm(self.buoys['Yellow - Entrance 2'].loc,
                                               self.buoys['Red - Exit 1'].loc))),
                             )/2


            
            way_last_dy = dim_length*.75*sin(self.vert_line_angle)
            way_last_dx = dim_length*.75*cos(self.vert_line_angle)

            way_last = Loc(center_pt1_y + cm_to_lat(way_last_dy),
                                center_pt1_x + cm_to_lon(center_pt1_y,way_last_dx))

            self.way1 = way_last
                
            # divide reaching distance into legs of max 500cm
            reachlength = loc_distance_cm(way_last, Loc(center_pt1_y,
                                                        center_pt1_x))
            spacing = 500
            numwaypoints = ceil(reachlength/spacing)

            deltalat = way_last.lat - center_pt1_y
            deltalon = way_last.lon - center_pt1_x

            self.angle_to_radius = atan2(deltalat, deltalon)
            latincr = cm_to_lat(spacing*sin(self.vert_line_angle))
            lonincr = cm_to_lon(center_pt1_y,spacing*cos(self.vert_line_angle))
            
            for iway in range(int(numwaypoints-1)):
                self.locs.append(
                    Loc(center_pt1_y+(iway+1)*latincr, 
                        center_pt1_x+(iway+1)*lonincr)
                    )
            self.locs.append(way_last)

        elif self.state == 2:  #------ StationSimple


            self.water.clearWaypoints() # clears waypoints 
            self.locs = []


            # reference pos is self.way1.

            # find center  line of box
            center_pt1_x = (self.buoys['Yellow - Entrance 1'].loc.lon +
                            self.buoys['Yellow - Entrance 2'].loc.lon)/2
            center_pt1_y = (self.buoys['Yellow - Entrance 1'].loc.lat +
                            self.buoys['Yellow - Entrance 2'].loc.lat)/2
            center_pt2_x = (self.buoys['Red - Exit 1'].loc.lon +
                            self.buoys['Red - Exit 2'].loc.lon)/2
            center_pt2_y = (self.buoys['Red - Exit 1'].loc.lat +
                            self.buoys['Red - Exit 2'].loc.lat)/2

            # angle and slope of line along forward path of box ("vertical")
            vert_line_slope = LineSlope(Loc(center_pt1_y,center_pt1_x),
                                        Loc(center_pt2_y,center_pt2_x))
            vert_line_angle = atan2( (center_pt2_y - center_pt1_y), 
                                     (center_pt2_x - center_pt1_x))

            logger.debug("vertical line slope is " + str(self.vert_line_slope));

            # angle and slope of "horizonal" lines
            if ( loc_distance_cm(self.buoys['Red - Exit 1'].loc,
                                   self.buoys['Yellow - Entrance 1'].loc) <
                   loc_distance_cm(self.buoys['Red - Exit 1'].loc,
                                   self.buoys['Yellow - Entrance 2'].loc)):
                horiz_line_slope = LineSlope(self.buoys['Red - Exit 1'].loc,
                                             self.buoys['Red - Exit 2'].loc)
                horiz_line_angle = LineAngle(self.buoys['Red - Exit 1'].loc,
                                             self.buoys['Red - Exit 2'].loc)
                logger.debug("box shape")
            else:
                horiz_line_slope = LineSlope(self.buoys['Red - Exit 2'].loc,
                                             self.buoys['Red - Exit 1'].loc)
                horiz_line_angle = LineAngle(self.buoys['Red - Exit 2'].loc,
                                             self.buoys['Red - Exit 1'].loc)
                logger.debug("x shape")

                

            dim_width = (loc_distance_cm(self.buoys['Red - Exit 1'].loc,
                                         self.buoys['Red - Exit 2'].loc))

            
            dx = 0.3 * dim_width
            logger.debug("horizontal line slope is " + str(self.horiz_line_slope));
            # convert to lat and lon
            dlat = cm_to_lat(dx*sin(horiz_line_angle))
            dlon = cm_to_lon(self.way1.lat,dx*cos(horiz_line_angle))



            way_A = Loc( self.way1.lat + dlat,
                         self.way1.lon + dlon)
            self.locs.append(way_A)

            way_B = Loc( self.way1.lat - dlat,
                         self.way1.lon - dlon)
            self.locs.append(way_B)



        elif self.state == 3:  #------ StationSimple


            self.water.clearWaypoints() # clears waypoints 
            self.locs = []

            # go to finish line

            # figure out point on finish line that doesn't require us to tack

            # find headings for the two close-hauled tacks
            (sbTack, pTack) = offset_heading(self.water.windDir, self.water.boat.noGo)

            logger.debug("Starboard tack heading is: " + str(sbTack))
            # find points of intersection with line defined by starting buoys
            x1 = self.buoys['Red - Exit 1'].loc.lon
            y1 = self.buoys['Red - Exit 1'].loc.lat
            x2 = self.buoys['Red - Exit 2'].loc.lon
            y2 = self.buoys['Red - Exit 2'].loc.lat

            x3 = self.water.boat.loc.lon
            y3 = self.water.boat.loc.lat
            x4 = self.water.boat.loc.lon + cm_to_lon(y3, 100)
            y4 = self.water.boat.loc.lat + cm_to_lat(100*tan(sbTack*pi/180))

            x5 = self.water.boat.loc.lon + cm_to_lon(y3, 100)
            y5 = self.water.boat.loc.lat + cm_to_lat(100*tan(pTack*pi/180))

            if ( (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4) == 0 or
                 (x1-x2)*(y3-y5) - (y1-y2)*(x3-x5) == 0):
                isect_x_sb = x1
                isect_x_p = x1
                isect_y_sb = y1
                isect_y_p = y1
            else:
                isect_x_sb = (( (x1*y2 - y1*x2)*(x3-x4) - (x1-x2)*(x3*y4 - y3*x4))/
                           ( (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4)))
                isect_x_p = (( (x1*y2 - y1*x2)*(x3-x5) - (x1-x2)*(x3*y5 - y3*x5))/
                           ( (x1-x2)*(y3-y5) - (y1-y2)*(x3-x5)))
                isect_y_sb = (( (x1*y2 - y1*x2)*(y3-y4) - (y1-y2)*(x3*y4 - y3*x4))/
                              ( (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4)))
                isect_y_p = (( (x1*y2 - y1*x2)*(y3-y5) - (y1-y2)*(x3*y5 - y3*x5))/
                             ( (x1-x2)*(y3-y5) - (y1-y2)*(x3-x5)))

            loc_sb = Loc(isect_y_sb, isect_x_sb)
            loc_p = Loc(isect_y_p, isect_x_p)


            if ( (isect_x_sb > x1 and isect_x_sb < x2) or
                 (isect_x_sb < x1 and isect_x_sb > x2)):
                # sb tack point is on start line (valid)
                if ( (isect_x_p > x1 and isect_x_p < x2) or
                     (isect_x_p < x1 and isect_x_p > x2)):
                    # port tack point is valid
                    
                    # then choose the closer one
                    # (alternatively, may want to choose the tack we're already on)
                    if (loc_distance_cm(loc_sb, self.water.boat.loc) >
                        loc_distance_cm(loc_p, self.water.boat.loc)):
                        self.endpoint = loc_p
                    else:
                        self.endpoint = loc_sb
                else:
                    # only the sb tack is valid
                    self.endpoint = loc_sb
            elif ( (isect_x_p > x1 and isect_x_p < x2) or
                   (isect_x_p < x1 and isect_x_p > x2)):
                # only port tack is valid
                self.endpoint = loc_p
            else:
                self.endpoint = self.buoys['Red - Exit 1'].loc # dummy location

            # otherwise go to nearest buoy within some safety margin:
            dist_start1 = loc_distance_cm(self.water.boat.loc, 
                                          self.buoys['Red - Exit 1'].loc)
            dist_start2 = loc_distance_cm(self.water.boat.loc, 
                                          self.buoys['Red - Exit 2'].loc)
            if dist_start1 < dist_start2:
                startpoint_nom = self.buoys['Red - Exit 1'].loc
                near_loc = self.buoys['Red - Exit 1'].loc
                far_loc = self.buoys['Red - Exit 2'].loc
                logger.debug('Closest to buoy 1')
            else:
                startpoint_nom = self.buoys['Red - Exit 2'].loc
                near_loc = self.buoys['Red - Exit 2'].loc
                far_loc = self.buoys['Red - Exit 1'].loc
                logger.debug('Closest to buoy 2')
                
            # offset from nominal endpoint to safe region in middle.
            offset = 500
            self.start_angle = atan2(
                far_loc.lat - near_loc.lat,
                far_loc.lon - near_loc.lon)
            
            self.endpoint_alt = Loc(
                startpoint_nom.lat + 
                cm_to_lat(offset * sin(self.start_angle)),
                startpoint_nom.lon + 
                cm_to_lon(y1,offset * cos(self.start_angle)))
                
            if ( loc_distance_cm(self.endpoint,self.buoys['Red - Exit 1'].loc) < 500 or
                 loc_distance_cm(self.endpoint,self.buoys['Red - Exit 2'].loc) < 500):
                self.locs.append(self.endpoint_alt)
            else:
                self.locs.append(self.endpoint)


            # clearance point
            clear = OffsetFromLineCenter(self.buoys['Red - Exit 1'].loc,
                                         self.buoys['Red - Exit 2'].loc,
                                         self.buoys['Yellow - Entrance 1'].loc,
                                         200)
            self.locs.append(clear)

        #----------------------------- StationSimple

        else:
            self.timecount = 0
            self.state = 0
            self.currentLocIndex = 0
            self.updateWaypoints()
        
        for idx, loc in enumerate(self.locs):
            logger.debug(str(idx) + " " + str(loc.lat))
            self.water.addWaypoint(WaypointItem(loc, idx+1))

    def currentWaypoint(self):
        # StationSimple
        self.doAnnotations()

        self.tacktime += 1

        numwaypoints = len(self.locs)

        logger.debug("waypoint index: " + str(self.currentLocIndex))
        disp.waypoint = self.currentLocIndex + 1
        logger.debug("state: " + str(self.state))
        logger.debug("start timer: " + str(self.starttimecount))
        logger.debug("time count: " + str(time.time() - self.timeref))
        logger.debug("time: " + str(self.timeref))
        disp.starttimecount = self.starttimecount/5
        disp.timecount = self.timecount/5 

        if self.timer_en == True:
            self.starttimecount += 1
            self.currentLocIndex = 0
            self.updateWaypoints()

        if self.box_timer == True:
            self.timecount += 1

        if self.state == -1:
            if time.time() - self.timeref > 60:
                self.state = 0
                self.currentLocIndex = 0
                self.updateWaypoints()
                self.timer_en = False
                self.box_timer = True
                self.starttimecount = 0
            elif loc_distance_cm(self.water.boat.loc, self.locs[self.currentLocIndex]) < 200:
                logger.debug("Reached waypoint: " + str(self.currentLocIndex))
                self.currentLocIndex +=1
                # if no more waypoints, increment state
                if self.currentLocIndex > numwaypoints - 1:
                    logger.debug("reached end of waypoint list")
                    self.updateWaypoints() # calculate new waypoints 
                    self.currentLocIndex = 0      # reset waypoint index


        elif self.state == 0:
            # if state 0, check if boat crosses the start line (and in the right direction)
            prev_side = self.sline_side
            self.sline_side = SameSideLine(self.buoys['Yellow - Entrance 1'].loc, 
                                           self.buoys['Yellow - Entrance 2'].loc,
                                           self.water.boat.loc,
                                           self.buoys['Red - Exit 1'].loc)

            if (prev_side == False) and (self.sline_side == True):
                # just crossed start line
                logger.debug("Crossed start line")

                if BetweenBuoys(self.buoys['Yellow - Entrance 1'].loc, 
                                self.buoys['Yellow - Entrance 2'].loc,
                                self.water.boat.loc):
                    # passed between buoys, who cares where waypoint was?
                    logger.debug("Between buoys")
                    self.state = 1
                    self.currentLocIndex = 0
                    self.locs = []
                    self.updateWaypoints()

                else:
                    # make this create new waypoints to guide around start marks
                    logger.debug("Missed start line!")

        elif  time.time() - self.timeref > 250:
            
            self.state = 3
            self.currentLocIndex = 0      # reset waypoint index
            self.updateWaypoints() # calculate new waypoints 

        elif self.state == 2:
            if loc_distance_cm(self.water.boat.loc, self.locs[self.currentLocIndex]) < 300:
                logger.debug("Reached waypoint: " + str(self.currentLocIndex))
                self.currentLocIndex += 1
                if self.currentLocIndex > numwaypoints - 1:
                    logger.debug("reached end of waypoint list")
                    self.currentLocIndex = 0      # reset waypoint index
                    self.updateWaypoints() # calculate new waypoints 


        # if close enough to waypoint, increment waypoint
        elif loc_distance_cm(self.water.boat.loc, self.locs[self.currentLocIndex]) < 100:
            logger.debug("Reached waypoint: " + str(self.currentLocIndex))
            if self.state == 1 and self.currentLocIndex == 0:
                self.tacktime = 0
                logger.debug("Reset tack time")
            else:
                self.lasttack = self.tacktime
                self.tacktime = 0 # restart tack time count
                logger.debug("last tack duration: " + str(self.lasttack))
            self.currentLocIndex +=1
            # if no more waypoints, increment state
            if self.currentLocIndex > numwaypoints - 1:
                logger.debug("reached end of waypoint list")
                self.state += 1            #increment state
                self.updateWaypoints() # calculate new waypoints 
                self.currentLocIndex = 0      # reset waypoint index


        return self.locs[self.currentLocIndex]

    def doAnnotations(self):
        # StationSimple
        self.water.clearAnnotations()

        # draw distance line for start buoys
        startLine = DistanceLine(self.water, self.buoys['Red - Exit 1'].loc, self.buoys['Red - Exit 2'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

        startLine = DistanceLine(self.water, self.buoys['Yellow - Entrance 1'].loc, self.buoys['Yellow - Entrance 2'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

        startLine = DistanceLine(self.water, self.buoys['Red - Exit 1'].loc, self.buoys['Yellow - Entrance 1'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

        startLine = DistanceLine(self.water, self.buoys['Red - Exit 2'].loc, self.buoys['Yellow - Entrance 2'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

