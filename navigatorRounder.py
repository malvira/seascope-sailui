import sys
import logging
logger = logging.getLogger(__name__)

from LatLon import *
from Heading import *
from Annotation import *
from NavExtra import *

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class navigatorRounder():
    def __init__(self, water, buoys):
        self.water = water
        self.buoys = buoys
        self.locs = []
        self.currentLocIndex = 0
        logger.debug("create rounder navigator")
        self.updateWaypoints()
    def updateWaypoints(self):
        """ recompute waypoint positions """
        self.water.clearWaypoints()
        key = self.buoys.keys()[0]
        self.loc = self.buoys['Yellow - 1'].loc
        dlat = cm_to_lat(300)
        dlon = cm_to_lon(self.loc.lat, 300)
        loc = self.loc
        
        self.locs = [
            Loc(loc.lat + dlat, loc.lon + dlon),
            Loc(loc.lat + dlat, loc.lon - dlon),
            Loc(loc.lat - dlat, loc.lon - dlon),
            Loc(loc.lat - dlat, loc.lon + dlon),
            ]
        
        for idx, loc in enumerate(self.locs):
            logger.debug(str(idx) + " " + str(loc.lat))
            self.water.addWaypoint(WaypointItem(loc, idx+1))

    def currentWaypoint(self):
        self.water.clearAnnotations()
        # test if the boat is within a certain distance of the current 
        if loc_distance_cm(self.water.boat.loc, self.locs[self.currentLocIndex]) < 100:
            self.currentLocIndex += 1
            if self.currentLocIndex > 3:
                self.currentLocIndex = 0
            logger.debug("waypoint index: " + str(self.currentLocIndex))
        return self.locs[self.currentLocIndex]


def SameSideLine(locA, locB, locC, locD):
    # return 1 if locC and locB are on the same side of line locA --> locB
    Ax = locA.lon
    Bx = locB.lon
    Cx = locC.lon
    Dx = locD.lon
    Ay = locA.lat
    By = locB.lat
    Cy = locC.lat
    Dy = locD.lat

    return (cmp( (Bx - Ax) * (Cy - Ay) - (By -Ay) * (Cx - Ax) , 0) ==
            cmp( (Bx - Ax) * (Dy - Ay) - (By -Ay) * (Dx - Ax) , 0))

def LineSlope(locA, locB):
    # return slope of line given by endpoints A and B
    Ax = locA.lon
    Bx = locB.lon
    Ay = locA.lat
    By = locB.lat


    if (Bx - Ax) == 0 :
        return 10000
    else:
        return (By - Ay)/(Bx - Ax)

def LineAngle(locA, locB):
    # return angle of line given by endpoints A and B
    Ax = locA.lon
    Bx = locB.lon
    Ay = locA.lat
    By = locB.lat


    return atan2(By - Ay,Bx - Ax)


def BetweenBuoys(locA,locB,locC):
    # true if locC is between buoys A and B

    # BUG: doesn't work if one of the points is at (0,0)

    Ax = locA.lon
    Bx = locB.lon
    Cx = locC.lon
    Ay = locA.lat
    By = locB.lat
    Cy = locC.lat

    return ( ( (Cx > Ax and Cx < Bx) or (Cx < Ax and Cx > Bx) ) and
             ( (Cy > Ay and Cy < By) or (Cy < Ay and Cy > By) ))


def ProjToLine(locA, locB, locC):
# projects point C onto line formed by A and B.
# doesn't guarantee that new point is between A and B
    Ax = locA.lon
    Bx = locB.lon
    Cx = locC.lon
    Ay = locA.lat
    By = locB.lat
    Cy = locC.lat

    line_dx = Bx - Ax
    line_dy = By - Ay

    line_slope = LineSlope(locA, locB)
    if line_slope == 0:
        proj_slope = 10000
    else:
        proj_slope = -1 / line_slope

    # find intersection point
    # t1 = ((y1-y2)*dx2 - (x1-x2)*dy2) / (dx1*dy2-dy1*dx2)

    if (line_dx*proj_slope - line_dy*1) == 0:
        t1 = 0
    else:
        t1 = (((locA.lat - Cy)*1 - (locA.lon -
                                    Cx) * proj_slope) / 
              (line_dx*proj_slope - line_dy*1))
            
    return Loc(locA.lat + t1 * line_dy,
               locA.lon + t1 * line_dx)
