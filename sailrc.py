#!/usr/bin/python
# -*- coding: utf-8 -*-

import signal
import sys
import socket
import time
import os
import math

from PyQt4 import QtGui
from PyQt4 import QtCore

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from boat import *
try:
    import coapy.connection
    import coapy.options
    import coapy.link
except ImportError:
    coapy = None
    print "coapy not installed"
    print "git clone git://coapy.git.sourceforge.net/gitroot/coapy/coapy"
    print "cd coapy; python setup.py install"

class SailRC(QtGui.QWidget):
  
    def __init__(self):
        super(SailRC, self).__init__()
        self.host = 'aaaa::250:c2a8:ca00:2'
        self.port = 61616
        self.address_family = socket.AF_INET6        
        self.led = 0
        self.lat = '0N'
        self.lon = '0E'
        self.initUI()

        lognum = 1
        while(os.path.exists('log-' + str(lognum) + '.csv')):
            lognum = lognum+1
        self.log = open('log-' + str(lognum) + '.csv', 'w')

    def initUI(self):

        self.setFocusPolicy(QtCore.Qt.StrongFocus)

        vbox = QtGui.QVBoxLayout()
        hbox = QtGui.QHBoxLayout()
        
        self.rudder = QtGui.QSlider(QtCore.Qt.Horizontal, self)
        self.rudder.setFocusPolicy(QtCore.Qt.NoFocus)
        self.rudder.setRange(-90,90)

        sailvbox = QVBoxLayout()
        self.sail = QtGui.QSlider(QtCore.Qt.Vertical, self)
        self.sail.setFocusPolicy(QtCore.Qt.NoFocus)
        self.sail.setRange(0,90)

        closeHauled = QSpinBox()
        closeHauled.setMaximum(90)
        closeHauled.setMinimum(0)
        
        sailvbox.addWidget(self.sail)
        sailvbox.addWidget(closeHauled)

        headingCommand = QtGui.QSlider(QtCore.Qt.Vertical, self)
        headingCommand.setFocusPolicy(QtCore.Qt.NoFocus)
        headingCommand.setRange(0,359)

        self.boat = BoatWidget()
        boat = self.boat
        self.rudder.valueChanged.connect(boat.setRudder)
        self.rudder.sliderMoved.connect(self.post_rudder)
        self.rudder.setValue(self.boat.rudderAngle)

        self.sail.valueChanged.connect(boat.setMainCommand)
        self.sail.setValue(self.boat.mainCommand)
        self.sail.sliderMoved.connect(self.post_sail)
        closeHauled.valueChanged.connect(boat.setCloseHauled)

        headingCommand.valueChanged.connect(boat.setHeadingCommand)
        headingCommand.setValue(self.boat.headingCommand)
        headingCommand.sliderMoved.connect(self.post_course)

        hbox.addWidget(headingCommand)
        hbox.addLayout(sailvbox)
        hbox.addWidget(boat)
        hbox.setStretchFactor(sailvbox,1)
        hbox.setStretchFactor(boat,100)

        host = QLineEdit()
        host.setText(self.host)

        update = QPushButton()
        update.setText("Update")
        update.clicked.connect(self.update)

        command = QPushButton()
        command.setText("Command")
        command.clicked.connect(self.post_all)

        vbox.addLayout(hbox)
        vbox.addWidget(self.rudder)
        vbox.addWidget(host)
        vbox.addWidget(update)
        vbox.addWidget(command)

        self.setLayout(vbox)

        host.textChanged.connect(self.hostChange)
                
        self.setWindowTitle('Sail RC')

    def hostChange(self, value):
        print "host: " + value
        self.host = value

    def toggleLED(self):
        if self.led:
            self.led = 0
            self.post_command('led',0)
        else:
            self.led = 1
            self.post_command('led',1)

    def post_course(self, value):
        self.post_command('course', value)

    def post_rudder(self, value):
        self.post_command('rudder', value)

    def post_sail(self, value):
        self.post_command('main', value)

    def post_command(self, prop, value):
        uri = "cmd_post?prop=" + prop
        valuestr = "value=" + str(value)
        print "post command " + uri + " " + str(value)
        try:
            ep = coapy.connection.EndPoint(address_family=self.address_family)
            msg = coapy.connection.Message(code=coapy.PUT, payload=valuestr, uri_path=uri)
            remote = (self.host, self.port, 0, 0)
            ep.send(msg,remote)
            ep.process(250)
        except:
            pass

    def post_all(self):
        props = ['rudder', 'course', 'main', 'led']

        for prop in props:
            if prop == 'main':
                self.post_command('main', self.boat.mainCommand)
            if prop == 'rudder':
                self.post_command('rudder', self.boat.rudderAngle)
            if prop == 'course':
                self.post_command('course', self.boat.headingCommand)
                            

    def update(self):
        resources = ['jib', 'rudder', 'wind_dir', 'wind_speed', 'heading', 'main', 'jib_command', 'latitude', 'longitude','compass']

        try:
            for uri in resources:
                remote = (self.host, self.port, 0, 0)
                req = coapy.connection.Message(code=coapy.GET, uri_path=uri)
                ep = coapy.connection.EndPoint(address_family=self.address_family)
                tx_rec = ep.send(req, remote)
                rv = None
                try:
                    rv = ep.process(250)
                except socket.error, (value,message):
                    print "Could not open socket: " + message
                    print "GET " + self.host + "/" + uri + " failed"

                if coapy.OK and (type(rv) is not 'NoneType'):
                    msg = rv.message

                    try:
                        if uri == 'latitude' or uri == 'longitude' or uri == 'compass':
                            if uri == 'latitude':
                                self.lat = msg.payload.rstrip()
                            if uri == 'longitude':
                                self.lon = msg.payload.rstrip()
                            if uri == 'compass':
                                print "Set Compass: " + msg.payload.rstrip()
                                heading = processCompass(msg.payload.rstrip().split(','))
                                self.boat.setHeading(heading)
                        else:
                            value = int(msg.payload)

                        if uri == 'jib':
                            print "Set jib: " + msg.payload.rstrip()
                            self.boat.setJib(int(msg.payload))
                        if uri == 'jib_limit':
                            print "Set jib limit: " + msg.payload.rstrip()
                            self.boat.setJibCommand(int(msg.payload))
                        if uri == 'rudder':
                            print "Set Rudder: " + msg.payload.rstrip()
                            self.boat.setRudder(int(msg.payload))
                            self.rudder.setValue(int(msg.payload))
                        if uri == 'wind_dir':
                            print "Set Wind Dir: " + msg.payload.rstrip()
                            self.boat.setWindD(int(msg.payload))
                        if uri == 'wind_speed':
                            print "Set Wind Speed: " + msg.payload.rstrip()
                            self.boat.setWindSpeed(int(msg.payload))
                        if uri == 'heading':
                            print "Set Heading: " + msg.payload.rstrip()
                            self.boat.setHeading(int(msg.payload))
                        if uri == 'main':
                            print "Set Main: " + msg.payload.rstrip()
                            self.boat.setMain(int(msg.payload))
                    except ValueError:
                        print "bad number returned: GET " + self.host + "/" + uri
        except:
            pass

        entry = str(time.time()) + ',' + self.lat + ',' + self.lon + ',' + str(self.boat.rudderAngle) + ',' + str(self.boat.jibAngle) + ',' + str(self.boat.jibCommand) + ',' + str(self.boat.mainAngle) + ',' + str(self.boat.mainCommand) + ',' + str(self.boat.windDirection) + ',' + str(self.boat.windSpeed) + ',' + str(self.boat.heading) + ',' + str(self.boat.headingCommand) + '\n' 
        self.log.write(entry)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Left:
            self.rudder.setValue(self.boat.setRudder(self.rudder.value()-1))
        if event.key() == QtCore.Qt.Key_Right:
            self.rudder.setValue(self.boat.setRudder(self.rudder.value()+1))
        if event.key() == QtCore.Qt.Key_Up:
            self.sail.setValue(self.sail.value()+1)
        if event.key() == QtCore.Qt.Key_Down:
            self.sail.setValue(self.sail.value()-1)

def signal_handler(signal, frame):
        sys.exit(0)

if __name__ == '__main__':

    app = QtGui.QApplication(sys.argv)
    rc = SailRC()
    rc.setFocus()

    updateTimer = QTimer()
    updateTimer.start(200)
    updateTimer.timeout.connect(rc.update)

    heartbeat = QTimer()
    heartbeat.start(1000)
    heartbeat.timeout.connect(rc.toggleLED)

    rc.show()
    rc.update()

    signal.signal(signal.SIGINT, signal_handler)

    app.exec_()
