import sys
import logging

from math import *
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from LatLon import *

class Obstacle(QGraphicsItem):
    def __init__(self, water, loc, wsid, radius=100):
        super(Obstacle, self).__init__()
        self.water = water
        self.loc = loc
        self.wsid = wsid
        self.radius = radius
        self.setZValue(-1)
    def boundingRect(self):
        return QRectF(0,0,self.water.cm_to_pix(self.radius*2),self.water.cm_to_pix(self.radius*2))
    def paint(self, qp, opt, widg):
        qp.setBrush(QBrush(QColor("gray")))
        qp.drawEllipse(0,0,self.water.cm_to_pix(self.radius*2),self.water.cm_to_pix(self.radius*2))
    def setPos(self, x, y):
        x -= self.water.cm_to_pix(self.radius)
        y -= self.water.cm_to_pix(self.radius)
        super(Obstacle, self).setPos(x,y)
