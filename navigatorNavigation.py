import sys
import logging
logger = logging.getLogger(__name__)

from LatLon import *
from Heading import *
from Annotation import *

from NavExtra import *

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *
        
class navigatorNavigation():
    def __init__(self, water, buoys):
        self.water = water
        self.buoys = buoys
        self.state = 0
        self.locs = []
        self.clear = 0
        self.timecount = 0
        self.sline_side = True
        self.currentLocIndex = 0
        logger.debug("create waypoint navigator")
        ### TODO keep a list of all waypoints 
        self.updateWaypoints()
    def updateWaypoints(self):
        """ recompute waypoint positions """
        self.water.clearWaypoints() # clears waypoints
        self.water.boat.loc
        start1loc = self.buoys['Red - Start 1'].loc
        start2loc = self.buoys['Red - Start 2'].loc
        
        slat = (start1loc.lat + start2loc.lat)/2
        slon = (start1loc.lon + start2loc.lon)/2
        logger.debug("center of start line latitude: " + str(slat))


        if self.state == -1:
            self.water.clearWaypoints() # clears waypoints
            self.locs = []

            [wayA, wayB] = OffsetReachBuoys(self.buoys['Red - Start 1'].loc,
                                            self.buoys['Red - Start 2'].loc,
                                            self.buoys['Red - Upwind Mark'].loc,
                                            300)
            self.locs.append(wayA)
            self.locs.append(wayB)
        

        elif self.state == 0:   # initial state -- at start
            self.water.clearWaypoints() # clears waypoints
            self.locs = []
            # make waypoint on start line

            self.locs.append(Loc(slat, slon))

        # Navigation
        elif self.state == 1:  # reaching to windward mark
            self.water.clearWaypoints() # clears waypoints 
            if self.clear:
                self.locs = []
                self.currentLocIndex = 0      # reset waypoint index
                self.clear = 1 # clear next time unless overridden
            
            # figure out how far away the windward mark is from
            # starting line in cm
            # initial mark is at Loc(slat, slon)

            deltalat_mark = self.buoys['Red - Upwind Mark'].loc.lat - slat
            deltalon_mark = self.buoys['Red - Upwind Mark'].loc.lon - slon
    
            # final waypoint should be 100cm to the "right" of the
            # windward mark
            radius = 300
            self.angle_to_mark = atan2(deltalat_mark, deltalon_mark)
            self.radius_angle = self.angle_to_mark + pi/2
            mark_radius_lat = cm_to_lat(radius*sin(self.radius_angle))
            mark_radius_lon = cm_to_lon(slat,radius*cos(self.radius_angle))
            self.mark_waypoint = Loc(
                self.buoys['Red - Upwind Mark'].loc.lat+mark_radius_lat,
                self.buoys['Red - Upwind Mark'].loc.lon+mark_radius_lon,
                )
                
            # divide reaching distance into legs of max 200cm
            reachlength = loc_distance_cm(self.mark_waypoint, Loc(slat, slon))
            spacing = 500;
            numwaypoints = ceil(reachlength/spacing)
            logger.debug(str(numwaypoints))
            deltalat = self.mark_waypoint.lat - slat
            deltalon = self.mark_waypoint.lon - slon

            self.angle_to_radius = atan2(deltalat, deltalon)
            latincr = cm_to_lat(spacing*sin(self.angle_to_radius))
            lonincr = cm_to_lon(slat,spacing*cos(self.angle_to_radius))
            
            for iway in range(int(numwaypoints-1)):
                self.locs.append(
                    Loc(slat+(iway+1)*latincr, slon+(iway+1)*lonincr)
                    )
            self.locs.append(self.mark_waypoint)

            # now do rounding, use radius value above
            # from existing waypoint 180 degrees around to opposite side
            
            numradiuspoints = 2
            r_delta_angle = pi / numradiuspoints

            for iway in range(int(numradiuspoints)):
                theta = fmod(self.radius_angle-r_delta_angle*(iway+1), 2*pi)

                r_dlat = cm_to_lat(radius * sin(theta))
                r_dlon = cm_to_lon(slat,radius * cos(theta))
                
                center = self.buoys['Red - Upwind Mark'].loc

                self.locs.append(Loc(
                        center.lat + r_dlat,
                        center.lon + r_dlon)
                                 )

        ##-------------------- Navigation
        elif self.state == 2:
            # reaching
            lastwaypoint = self.locs[self.currentLocIndex-1]

            self.water.clearWaypoints() # clears waypoints 
            if self.clear:
                self.locs = []
                self.currentLocIndex = 0      # reset waypoint index                
                self.clear = 1

            cur_x = self.water.boat.loc.lon
            cur_y = self.water.boat.loc.lat

            sline_dx = self.buoys['Red - Start 1'].loc.lon - self.buoys['Red - Start 2'].loc.lon
            sline_dy = self.buoys['Red - Start 1'].loc.lat - self.buoys['Red - Start 2'].loc.lat
            sline_origin = self.buoys['Red - Start 1'].loc

            prj_way = ProjToLine(self.buoys['Red - Start 1'].loc,
                                 self.buoys['Red - Start 2'].loc,
                                 self.water.boat.loc)
            
            if BetweenBuoys(self.buoys['Red - Start 1'].loc,
                            self.buoys['Red - Start 2'].loc,
                            prj_way):
                self.locs.append(prj_way)
            else:
                logger.debug("Sail to closest buoy")
                # otherwise go to nearest buoy within some safety margin:
                dist_start1 = loc_distance_cm(self.water.boat.loc, 
                                              self.buoys['Red - Start 1'].loc)
                dist_start2 = loc_distance_cm(self.water.boat.loc, 
                                              self.buoys['Red - Start 2'].loc)
                if dist_start1 < dist_start2:
                    startpoint_nom = self.buoys['Red - Start 1'].loc
                    near_loc = self.buoys['Red - Start 1'].loc
                    far_loc = self.buoys['Red - Start 2'].loc
                    logger.debug('Closest to buoy 1')
                else:
                    startpoint_nom = self.buoys['Red - Start 2'].loc
                    near_loc = self.buoys['Red - Start 2'].loc
                    far_loc = self.buoys['Red - Start 1'].loc
                    logger.debug('Closest to buoy 2')

                # offset from nominal endpoint to safe region in middle.
                offset = 100
                self.start_angle = atan2(
                    far_loc.lat - near_loc.lat,
                    far_loc.lon - near_loc.lon)
            
                self.locs.append(Loc(
                        startpoint_nom.lat + 
                        cm_to_lat(offset * sin(self.start_angle)),
                        startpoint_nom.lon + 

                        cm_to_lon(cur_y,offset * cos(self.start_angle))))
            
                clear = OffsetFromLineCenter(self.buoys['Red - Start 1'].loc,
                                             self.buoys['Red - Start 2'].loc,
                                             self.buoys['Red - Upwind Mark'].loc,
                                             200)
                self.locs.append(clear)

                      ##--------------------  Navigation
        elif self.state > 2:
            self.state = 0  
            self.timecount = 0
            self.locs = []
            self.currentLocIndex = 0      # reset waypoint index
            self.updateWaypoints()

        ## Add Waypoints:
        for idx, loc in enumerate(self.locs):
            self.water.addWaypoint(WaypointItem(loc, idx+1))

    def currentWaypoint(self): # returns where the boat is trying to go
        # Navigation 
        self.doAnnotations()

        self.timecount += 1

        numwaypoints = len(self.locs)

        logger.debug("waypoint index: " + str(self.currentLocIndex))
        logger.debug("state: " + str(self.state))

        
        if self.state == -1:
            if loc_distance_cm(self.water.boat.loc, self.locs[self.currentLocIndex]) < 100:
                logger.debug("Reached waypoint: " + str(self.currentLocIndex))
                self.currentLocIndex +=1
                # if no more waypoints, increment state                      
                if self.currentLocIndex > numwaypoints - 1:
                    logger.debug("reached end of waypoint list")
                    self.updateWaypoints() # calculate new waypoints         
                    self.currentLocIndex = 0      # reset waypoint index     

        elif self.state == -1 and 200 - self.timecount == 0:
            self.state = 0
            self.currentLocIndex = 0
            self.updateWaypoints()
            

        elif self.state == 0:
            # if state 0, check if boat crosses the start line (and in the right direction)
            prev_side = self.sline_side
            self.sline_side = SameSideLine(self.buoys['Red - Start 1'].loc, 
                                           self.buoys['Red - Start 2'].loc,
                                           self.water.boat.loc,
                                           self.buoys['Red - Upwind Mark'].loc)

            if (prev_side == False) and (self.sline_side == True):
                # just crossed start line
                logger.debug("Crossed start line")

                if BetweenBuoys(self.buoys['Red - Start 1'].loc, 
                                self.buoys['Red - Start 2'].loc,
                                self.water.boat.loc):
                    # passed between buoys, who cares where waypoint was?
                    logger.debug("Between buoys")
                    self.state = 1
                    self.currentLocIndex += 1
                    self.updateWaypoints()
                else:
                    # make this create new waypoints to guide around start marks
                    logger.debug("Missed start line!")

        elif loc_distance_cm(self.water.boat.loc, self.locs[self.currentLocIndex]) < 100:
            logger.debug("Reached waypoint: " + str(self.currentLocIndex))
            self.currentLocIndex += 1
            # if no more waypoints, increment state
            if self.currentLocIndex > numwaypoints - 1:
                logger.debug("reached end of waypoint list")
                self.state += 1            #increment state
                self.updateWaypoints() # calculate new waypoints 
#                self.currentLocIndex = 0      # reset waypoint index


        return self.locs[self.currentLocIndex]

    def doAnnotations(self):
        self.water.clearAnnotations()

        # draw distance line for start buoys
        startLine = DistanceLine(self.water, self.buoys['Red - Start 1'].loc, self.buoys['Red - Start 2'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

        startLine = DistanceLine(self.water, self.buoys['Yellow - Exit 1'].loc, self.buoys['Red - Start 1'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

        startLine = DistanceLine(self.water, self.buoys['Yellow - Exit 2'].loc, self.buoys['Red - Start 2'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

        start1loc = self.buoys['Red - Start 1'].loc
        start2loc = self.buoys['Red - Start 2'].loc        
        slat = (start1loc.lat + start2loc.lat)/2
        slon = (start1loc.lon + start2loc.lon)/2

        startLine = DistanceLine(self.water, self.buoys['Red - Upwind Mark'].loc, Loc(slat, slon))
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)
