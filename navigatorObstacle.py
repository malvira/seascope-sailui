import sys
import logging
logger = logging.getLogger(__name__)

from LatLon import *
from Heading import *
from Annotation import *
from NavExtra import *

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class navigatorObstacle():
    def __init__(self, water, buoys):
        self.water = water
        self.buoys = buoys
        self.state = 0
        self.locs = []
        self.currentLocIndex = 0
        logger.debug("create waypoint navigator")
        ### TODO keep a list of all waypoints 
        self.updateWaypoints()

    def updateWaypoints(self):
        """ recompute waypoint positions """
        self.water.clearWaypoints()
        self.sline_side = True

        self.obstacle_radius = 100
        self.obstacle_margin = 300
        self.buoy_margin = 300

        if self.state == -1:
            self.water.clearWaypoints() # clears waypoints                         
            self.locs = []

            [wayA, wayB] = OffsetReachBuoys(self.buoys['Red - Start 1'].loc,
                                            self.buoys['Red - Start 2'].loc,
                                            self.buoys['Yellow - 1'].loc,
                                            600)
            self.locs.append(wayA)
            self.locs.append(wayB)

        elif self.state == 0:   # initial state -- at start        
            self.water.clearWaypoints() # clears waypoints         
            self.locs = []
            # make waypoint on start line                          

            start1loc = self.buoys['Red - Start 1'].loc
            start2loc = self.buoys['Red - Start 2'].loc

            slat = (start1loc.lat + start2loc.lat)/2
            slon = (start1loc.lon + start2loc.lon)/2

            self.locs.append(Loc(slat, slon))

        elif self.state == 1: # go to side of obstacle

            prevloc = self.locs[self.currentLocIndex - 1]

            self.water.clearWaypoints()
            self.locs = []
            self.currentLocIndex = 0


            start1loc = self.buoys['Red - Start 1'].loc
            start2loc = self.buoys['Red - Start 2'].loc

            slat = (start1loc.lat + start2loc.lat)/2
            slon = (start1loc.lon + start2loc.lon)/2

            startloc = Loc(slat, slon)

            self.angle_to_mark = LineAngle(self.buoys['White - Obstacle'].loc,
                                           prevloc)
            self.angle_mark_way1 = self.angle_to_mark + pi/2

            mark_radius_lat = cm_to_lat(self.buoy_margin *
                                        sin(self.angle_mark_way1))
            mark_radius_lon = cm_to_lon(prevloc.lat, self.buoy_margin *
                                        cos(self.angle_mark_way1))

            self.obstacle_way1 = Loc(
                self.buoys['White - Obstacle'].loc.lat + mark_radius_lat,
                self.buoys['White - Obstacle'].loc.lon + mark_radius_lon)

            reach_ways = ReachFromTo(startloc,
                                     self.obstacle_way1)
            for way in reach_ways:
                self.locs.append(way)
            self.locs.append(self.obstacle_way1)
            
        elif self.state == 2: # go to and around buoy

            prevloc = self.locs[self.currentLocIndex - 1]

            self.water.clearWaypoints()
            self.locs = []
            self.currentLocIndex = 0

            self.angle_to_mark = LineAngle(self.buoys['Yellow - 1'].loc, prevloc)
            mark_offset_lat = cm_to_lat(self.buoy_margin *
                                        sin(self.angle_to_mark + pi/2))
            mark_offset_lon = cm_to_lon(prevloc.lat, self.buoy_margin *
                                        cos(self.angle_to_mark + pi/2))
            self.mark_way1 = Loc(
                self.buoys['Yellow - 1'].loc.lat + mark_offset_lat,
                self.buoys['Yellow - 1'].loc.lon + mark_offset_lon)
                
            reach_ways = ReachFromTo(prevloc, self.mark_way1)
            for way in reach_ways:
                self.locs.append(way)
  #          self.locs.append(self.mark_way1)

            # now round the mark

            numradiuspoints = 3
            r_delta_angle = (180*pi/180) / numradiuspoints

            for iway in range(int(numradiuspoints)):
                theta = fmod((self.angle_to_mark + pi/2)
                             -r_delta_angle*(iway+1), 2*pi)

                r_dlat = cm_to_lat(self.buoy_margin * sin(theta))
                r_dlon = cm_to_lon(prevloc.lat,self.buoy_margin * cos(theta))

                center = self.buoys['Yellow - 1'].loc

                self.locs.append(Loc(
                        center.lat + r_dlat,
                        center.lon + r_dlon)
                                 )



        elif self.state == 3: # go around obstacle

            prevloc = self.locs[self.currentLocIndex - 1]

            self.water.clearWaypoints()
            self.locs = []
            self.currentLocIndex = 0


            self.angle_to_mark = LineAngle(self.buoys['White - Obstacle'].loc,
                                           prevloc)
            self.angle_mark_way1 = self.angle_to_mark + pi/2

            mark_radius_lat = cm_to_lat(self.buoy_margin *
                                        sin(self.angle_mark_way1))
            mark_radius_lon = cm_to_lon(prevloc.lat, self.buoy_margin *
                                        cos(self.angle_mark_way1))

            self.obstacle_way1 = Loc(
                self.buoys['White - Obstacle'].loc.lat + mark_radius_lat,
                self.buoys['White - Obstacle'].loc.lon + mark_radius_lon)

            reach_ways = ReachFromTo(prevloc,
                                     self.obstacle_way1)
            for way in reach_ways:
                self.locs.append(way)
            self.locs.append(self.obstacle_way1)

        elif self.state == 4: # go to finish line

            prevloc = self.locs[self.currentLocIndex - 1]

            self.water.clearWaypoints()
            self.locs = []
            self.currentLocIndex = 0


            start1loc = self.buoys['Red - Start 1'].loc
            start2loc = self.buoys['Red - Start 2'].loc

            slat = (start1loc.lat + start2loc.lat)/2
            slon = (start1loc.lon + start2loc.lon)/2


            reach_ways = ReachFromTo(prevloc,
                                     Loc(slat, slon))
            for way in reach_ways:
                self.locs.append(way)
            self.locs.append(Loc(slat, slon))
                                     

            # clearance point                                                      
            clear = OffsetFromLineCenter(self.buoys['Red - Start 1'].loc,
                                         self.buoys['Red - Start 2'].loc,
                                         self.buoys['Yellow - 1'].loc,
                                                                  200)
            self.locs.append(clear)


        elif self.state > 4:

            self.state = 0
            self.locs = []
            self.currentLocIndex = 0
            self.updateWaypoints()



        # Add waypoints:
        for idx, loc in enumerate(self.locs):
            self.water.addWaypoint(WaypointItem(loc, idx+1))
    def currentWaypoint(self):
        self.doAnnotations()
        numwaypoints = len(self.locs)

        logger.debug("waypoint index: " + str(self.currentLocIndex))
        logger.debug("state: " + str(self.state))

        
        if self.state == -1:
            if loc_distance_cm(self.water.boat.loc, self.locs[self.currentLocIndex]) < 200:
                logger.debug("Reached waypoint: " + str(self.currentLocIndex))
                self.currentLocIndex +=1
                # if no more waypoints, increment state                      
                if self.currentLocIndex > numwaypoints - 1:
                    logger.debug("reached end of waypoint list")
                    self.updateWaypoints() # calculate new waypoints         
                    self.currentLocIndex = 0      # reset waypoint index     

        elif self.state == 0:
            # if state 0, check if boat crosses the start line (and in the right direction)
            prev_side = self.sline_side
            self.sline_side = DiffSideLine(self.buoys['Red - Start 1'].loc, 
                                           self.buoys['Red - Start 2'].loc,
                                           self.water.boat.loc,
                                           self.buoys['Yellow - 1'].loc)

            if (prev_side == True) and (self.sline_side == False):
                # just crossed start line
                logger.debug("Crossed start line")

                if BetweenBuoys(self.buoys['Red - Start 1'].loc, 
                                self.buoys['Red - Start 2'].loc,
                                self.water.boat.loc):
                    # passed between buoys, who cares where waypoint was?
                    logger.debug("Between buoys")
                    self.state = 1
                    self.currentLocIndex += 1
                    self.updateWaypoints()
                else:
                    # make this create new waypoints to guide around start marks
                    logger.debug("Missed start line!")

        elif loc_distance_cm(self.water.boat.loc, self.locs[self.currentLocIndex]) < 200:
            logger.debug("Reached waypoint: " + str(self.currentLocIndex))
            self.currentLocIndex += 1
            # if no more waypoints, increment state
            if self.currentLocIndex > numwaypoints - 1:
                logger.debug("reached end of waypoint list")
                self.state += 1            #increment state
                self.updateWaypoints() # calculate new waypoints 
                self.currentLocIndex = 0      # reset waypoint index

        return self.locs[self.currentLocIndex]



    def doAnnotations(self):
        self.water.clearAnnotations()

        # draw distance line for start buoys
        startLine = DistanceLine(self.water, self.buoys['Red - Start 1'].loc, self.buoys['Red - Start 2'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

        startLine = DistanceLine(self.water, self.buoys['White - Obstacle'].loc, self.buoys['Yellow - 1'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

        start1loc = self.buoys['Red - Start 1'].loc
        start2loc = self.buoys['Red - Start 2'].loc        
        slat = (start1loc.lat + start2loc.lat)/2
        slon = (start1loc.lon + start2loc.lon)/2

        startLine = DistanceLine(self.water, self.buoys['White - Obstacle'].loc, Loc(slat, slon))
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)
