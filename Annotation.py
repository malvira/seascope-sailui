import sys
import logging

from math import *
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from LatLon import *

class DistanceLineFoo(QGraphicsItem):
    def __init__(self, water, locA, locB):
        super(DistanceLine, self).__init__()
        self.water = water
 #       self.water.scene.addItem(self)
    def boundingRect(self):
        return QRectF(0,0,100,100)
    def paint(self, qp, opt, widg):
#        dax, day = self.water.loc_to_xy(locA)
#        dbx, dby = self.water.loc_to_xy(locB)
        qp.drawLine(0,0,-20,-20)
#        qp.drawLine(dax, day, dbx, dby)

class DistanceLine(QGraphicsItem):
    def __init__(self, water, locA, locB):
        super(DistanceLine, self).__init__()
        self.water = water
        self.locA = locA
        self.locB = locB
    def boundingRect(self):
        return QRectF(0,0,400,400)
    def paint(self, qp, opt, widg):
        dax, day = self.water.loc_to_xy(self.locA)
        dbx, dby = self.water.loc_to_xy(self.locB)
        qp.drawLine(dax, day, dbx, dby)
        dist = loc_distance_cm(self.locA, self.locB)
        tx = (dax - dbx)/2 + dbx
        ty = (day - dby)/2 + dby
        qp.drawText(tx,ty,QString(str(int(dist))))

