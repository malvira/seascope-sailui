import sys
import logging
import copy
import math
logger = logging.getLogger(__name__)

from collections import deque

from LatLon import *
from Heading import *

def trimSail(boatItem):
    (l, r) = subtract_headings(180, boatItem.boat.windDirection)
    trim = (180 - min(l,r)) / 2
    logger.debug("sail trim: " + str(trim))
    boatItem.boat.mainCommand = trim

class controlManual():
    def __init__(self, bouyList):
        logger.debug("create manual controller")
        self.lastLoc = Loc(0,0)
        self.post_ok = True
        self.headingEstimator = GPSHeadingEstimator(2) # pass the number of unquie locs to store

    def updateBoat(self, boatItem, waypoint, water):
        # save this loc
        self.lastLoc = copy.copy(boatItem.loc)
        # set the rudder --- need to do this before calling update as the update can reset the rudder
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)
        # call boat.update
        boatItem.update()

        boatItem.setHeading(self.headingEstimator.heading(boatItem.loc))
        (l, r) = offset_heading(boatItem.boat.heading,boatItem.boat.windDirection)
        water.setWindDir(r)
        trimSail(boatItem)

        logger.debug("boat heading: " + str(boatItem.boat.heading))
        logger.debug("locs to heading: " + str(locs_to_heading(self.lastLoc, boatItem.loc)))
        # change boat parameters
        # set the rudder
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)
            boatItem.post_sail(boatItem.boat.mainCommand)

class controlRudderCompassWind():
    def __init__(self, bouyList):
        logger.debug("create rudder + gps controller")
        self.lastLoc = Loc(0,0)
        self.post_ok = True
    def updateBoat(self, boatItem, waypoint, water):
        #save this loc
        self.lastLoc = copy.copy(boatItem.loc)
        # set the rudder --- need to do this before calling update as the update can reset the rudder
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)

        # call boat.update
        boatItem.update()

        boatItem.setHeading(boatItem.boat.compass)
        (l, r) = offset_heading(boatItem.boat.heading,boatItem.boat.windDirection)
        water.setWindDir(r)

        self.windHeading = boatItem.boat.windDirection
        trimSail(boatItem)

        # how close the boat can sail in degrees
        boatItem.noGo = 40 
        (sbTack, pTack) = offset_heading(self.windHeading, boatItem.noGo);
        logger.debug("port tack heading " + str(pTack))
        logger.debug("starboard tack heading " + str(sbTack))

        headingToWaypoint = locs_to_heading(boatItem.loc, waypoint)

        # compute tack margins
        (sbMargin,x) = subtract_headings(sbTack, headingToWaypoint) 
        (x,pMargin) = subtract_headings(pTack, headingToWaypoint) 
        logger.debug("port tack margin " + str(pMargin))
        logger.debug("starboard tack margin " + str(sbMargin))

        #if waypoint is in no-go zone, choose ptack or sbtack based on tack margins
        #required margin to perform a tack
        tackMargin = 10
        if (heading_within_headings(headingToWaypoint, pTack, sbTack)) or (pMargin < tackMargin) or (sbMargin < tackMargin):
            (l,r) = subtract_headings(boatItem.boat.heading, headingToWaypoint)
            if min(l,r) == l:
                course = pTack
            else:
                course = sbTack
        else:
                course = headingToWaypoint

        (l, r) = subtract_headings(boatItem.boat.heading, course)
        if(l > r):
            # negative rudder turns right
            headingError = -r
        else:
            # positive rudder turns left
            headingError = l
        logger.debug("course: " + str(course))
        logger.debug("left error: " + str(l))
        logger.debug("right error: " + str(r))
        logger.debug("heading error: " + str(headingError))


        #command the rudder
        command = float(headingError) * 0.5

        # saturate at +-90
        if command > 90:
            command = 90
        if command < -90:
            command = -90

        # set the rudder
        boatItem.setRudder(int(command))

        # post to the boat
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)
            boatItem.post_sail(boatItem.boat.mainCommand)

class controlRudderCompass():
    def __init__(self, bouyList):
        logger.debug("create rudder + gps controller")
        self.lastLoc = Loc(0,0)
        self.post_ok = True

    def updateBoat(self, boatItem, waypoint, water):
        #save this loc
        self.lastLoc = copy.copy(boatItem.loc)
        # set the rudder --- need to do this before calling update as the update can reset the rudder
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)

        # call boat.update
        boatItem.update()

        boatItem.setHeading(boatItem.boat.compass)

        self.windHeading = water.windDir

        # how close the boat can sail in degrees
        boatItem.noGo = 40 
        (sbTack, pTack) = offset_heading(self.windHeading, boatItem.noGo);
        logger.debug("port tack heading " + str(pTack))
        logger.debug("starboard tack heading " + str(sbTack))

        headingToWaypoint = locs_to_heading(boatItem.loc, waypoint)

        # compute tack margins
        (sbMargin,x) = subtract_headings(sbTack, headingToWaypoint) 
        (x,pMargin) = subtract_headings(pTack, headingToWaypoint) 
        logger.debug("port tack margin " + str(pMargin))
        logger.debug("starboard tack margin " + str(sbMargin))

        #if waypoint is in no-go zone, choose ptack or sbtack based on tack margins
        #required margin to perform a tack
        tackMargin = 10
        if (heading_within_headings(headingToWaypoint, pTack, sbTack)) or (pMargin < tackMargin) or (sbMargin < tackMargin):
            (l,r) = subtract_headings(boatItem.boat.heading, headingToWaypoint)
            if min(l,r) == l:
                course = pTack
            else:
                course = sbTack
        else:
                course = headingToWaypoint

        (l, r) = subtract_headings(boatItem.boat.heading, course)
        if(l > r):
            # negative rudder turns right
            headingError = -r
        else:
            # positive rudder turns left
            headingError = l
        logger.debug("course: " + str(course))
        logger.debug("left error: " + str(l))
        logger.debug("right error: " + str(r))
        logger.debug("heading error: " + str(headingError))


        #command the rudder
        command = float(headingError) * 0.5

        # saturate at +-90
        if command > 90:
            command = 90
        if command < -90:
            command = -90

        # set the rudder
        boatItem.setRudder(int(command))

        # post to the boat
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)

class GPSHeadingEstimator():
    def __init__(self, maxlocs=2):
        self.locs = deque()
        self.maxLocs = maxlocs
        self.locs.append(Loc(0.0,0.0))
    def heading(self,loc):
        if self.locs[-1].lat != loc.lat and self.locs[-1].lon != loc.lon:
            if len(self.locs) > self.maxLocs:
                self.locs.popleft()
            self.locs.append(Loc(loc.lat, loc.lon))
        head = 0
        for i in range(len(self.locs)-1):
            head += locs_to_heading(self.locs[i], self.locs[i+1])            
        try:
            head = head/(len(self.locs)-1)
        except ZeroDivisionError:
            head = 0
        return int(head)


class controlRudderGPSWind():
    def __init__(self, bouyList):
        logger.debug("create rudder + gps controller")
        self.lastLoc = Loc(0,0)
        self.post_ok = True
        self.headingEstimator = GPSHeadingEstimator(2) # pass the number of unquie locs to store
    def updateBoat(self, boatItem, waypoint, water):
        #save this loc
        self.lastLoc = copy.copy(boatItem.loc)
        # set the rudder --- need to do this before calling update as the update can reset the rudder
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)

        # call boat.update
        boatItem.update()

        boatItem.boat.heading = self.headingEstimator.heading(boatItem.loc)

        (l, r) = offset_heading(boatItem.boat.heading,boatItem.boat.windDirection)
        water.setWindDir(r)

        logger.debug("boat heading " + str(boatItem.boat.heading))
        headingToWaypoint = locs_to_heading(boatItem.loc, waypoint)
        logger.debug("heading to waypoint " + str(headingToWaypoint))

        # compute number of degrees away going to the left and right
        # use the smaller one
        # be careful around 360/0

        self.windHeading = water.windDir
        # how close the boat can sail in degrees
        boatItem.noGo = 40 
        (sbTack, pTack) = offset_heading(self.windHeading, boatItem.noGo);
        logger.debug("port tack heading " + str(pTack))
        logger.debug("starboard tack heading " + str(sbTack))

        # compute tack margins
        (sbMargin,x) = subtract_headings(sbTack, headingToWaypoint) 
        (x,pMargin) = subtract_headings(pTack, headingToWaypoint) 
        logger.debug("port tack margin " + str(pMargin))
        logger.debug("starboard tack margin " + str(sbMargin))

        #if waypoint is in no-go zone, choose ptack or sbtack based on tack margins
        #required margin to perform a tack
        tackMargin = 10
        if (heading_within_headings(headingToWaypoint, pTack, sbTack)) or (pMargin < tackMargin) or (sbMargin < tackMargin):
            (l,r) = subtract_headings(boatItem.boat.heading, headingToWaypoint)
            if min(l,r) == l:
                course = pTack
            else:
                course = sbTack
        else:
                course = headingToWaypoint

        (l, r) = subtract_headings(boatItem.boat.heading, course)
        if(l > r):
            # negative rudder turns right
            headingError = -r
        else:
            # positive rudder turns left
            headingError = l
        logger.debug("course: " + str(course))
        logger.debug("left error: " + str(l))
        logger.debug("right error: " + str(r))
        logger.debug("heading error: " + str(headingError))


        #command the rudder
        command = float(headingError) * 0.5

        # saturate at +-90
        if command > 90:
            command = 90
        if command < -90:
            command = -90

        # set the rudder
        boatItem.setRudder(int(command))

        # post to the boat
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)

class controlRudderGPS():
    def __init__(self, bouyList):
        logger.debug("create rudder + gps controller")
        self.lastLoc = Loc(0,0)
        self.post_ok = True
        self.headingEstimator = GPSHeadingEstimator(2) # pass the number of unquie locs to store
    def updateBoat(self, boatItem, waypoint, water):
        #save this loc
        self.lastLoc = copy.copy(boatItem.loc)
        # set the rudder --- need to do this before calling update as the update can reset the rudder
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)
            boatItem.post_sail(60)

        # call boat.update
        boatItem.update()

        boatItem.boat.heading = self.headingEstimator.heading(boatItem.loc)

        logger.debug("boat heading " + str(boatItem.boat.heading))
        headingToWaypoint = locs_to_heading(boatItem.loc, waypoint)
        logger.debug("heading to waypoint " + str(headingToWaypoint))

        # compute number of degrees away going to the left and right
        # use the smaller one
        # be careful around 360/0


        self.windHeading = water.windDir
        # how close the boat can sail in degrees
        boatItem.noGo = 40 
        (sbTack, pTack) = offset_heading(self.windHeading, boatItem.noGo);
        logger.debug("port tack heading " + str(pTack))
        logger.debug("starboard tack heading " + str(sbTack))

        # compute tack margins
        (sbMargin,x) = subtract_headings(sbTack, headingToWaypoint) 
        (x,pMargin) = subtract_headings(pTack, headingToWaypoint) 
        logger.debug("port tack margin " + str(pMargin))
        logger.debug("starboard tack margin " + str(sbMargin))

        #if waypoint is in no-go zone, choose ptack or sbtack based on tack margins
        #required margin to perform a tack
        tackMargin = 10
        if (heading_within_headings(headingToWaypoint, pTack, sbTack)) or (pMargin < tackMargin) or (sbMargin < tackMargin):
            (l,r) = subtract_headings(boatItem.boat.heading, headingToWaypoint)
            if min(l,r) == l:
                course = pTack
            else:
                course = sbTack
        else:
                course = headingToWaypoint

        (l, r) = subtract_headings(boatItem.boat.heading, course)
        if(l > r):
            # negative rudder turns right
            headingError = -r
        else:
            # positive rudder turns left
            headingError = l
        logger.debug("course: " + str(course))
        logger.debug("left error: " + str(l))
        logger.debug("right error: " + str(r))
        logger.debug("heading error: " + str(headingError))


        #command the rudder
        command = float(headingError) * 0.5

        # saturate at +-90
        if command > 90:
            command = 90
        if command < -90:
            command = -90

        # set the rudder
        boatItem.setRudder(int(command))

        # post to the boat
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)
            boatItem.post_sail(60)


class controlRudderGPSnoTack():
    def __init__(self, bouyList):
        logger.debug("create rudder + gps controller")
        self.lastLoc = Loc(0,0)
        self.post_ok = True
    def updateBoat(self, boatItem, waypoint):
        #save this loc
        self.lastLoc = copy.copy(boatItem.loc)
        # set the rudder --- need to do this before calling update as the update can reset the rudder
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)
            boatItem.post_sail(60)

        # call boat.update
        boatItem.update()
        boatItem.boat.heading = locs_to_heading(self.lastLoc, boatItem.loc)
        logger.debug("boat heading " + str(boatItem.boat.heading))
        headingToWaypoint = locs_to_heading(boatItem.loc, waypoint)
        logger.debug("heading to waypoint " + str(headingToWaypoint))

        # compute number of degrees away going to the left and right
        # use the smaller one
        # be careful around 360/0

        #left error
        if(headingToWaypoint > boatItem.boat.heading):
            leftError = boatItem.boat.heading + (360 - headingToWaypoint)
        else:
            leftError = boatItem.boat.heading - headingToWaypoint

        #right error
        if(headingToWaypoint < boatItem.boat.heading):
            rightError = 360 - boatItem.boat.heading + headingToWaypoint
        else:
            rightError = headingToWaypoint - boatItem.boat.heading
        if(leftError > rightError):
            headingError = rightError
        else:
            headingError = -leftError
        logger.debug("left error: " + str(leftError))
        logger.debug("right error: " + str(rightError))
        logger.debug("heading error: " + str(headingError))

        #command the ruddber
        command = headingError

        # saturate at +-90
        if command > 90:
            command = 90
        if command < -90:
            command = -90

        # set the rudder
        boatItem.setRudder(int(command))

        # post to the boat
        if self.post_ok:
            boatItem.post_rudder(boatItem.boat.rudderAngle)
            boatItem.post_sail(60)
