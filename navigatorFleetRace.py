import sys
import logging
logger = logging.getLogger(__name__)

from LatLon import *
from Heading import *
from Annotation import *
from NavExtra import *

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class navigatorFleetRace():
    def __init__(self, water, buoys):
        self.water = water
        self.buoys = buoys
        self.state = 0
        self.clear = 0
        self.locs = []
        self.timecount = 0
        self.timer_en = False
        self.currentLocIndex = 0
        logger.debug("create waypoint navigator")
        ### TODO keep a list of all waypoints 
        self.updateWaypoints()
    def updateWaypoints(self):
        """ recompute waypoint positions """
        self.water.clearWaypoints() # clears waypoints
        self.water.boat.loc
        start1loc = self.buoys['Red - Start 1'].loc
        start2loc = self.buoys['Red - Start 2'].loc

        self.sline_side = True
        
        slat = (start1loc.lat + start2loc.lat)/2
        slon = (start1loc.lon + start2loc.lon)/2
        logger.debug("center of start line latitude: " + str(slat))
        
        if self.state == -1:
            self.water.clearWaypoints() # clears waypoints  
            self.locs = []

            [wayA, wayB] = OffsetReachBuoys(self.buoys['Red - Start 1'].loc,
                                            self.buoys['Red - Start 2'].loc,
                                            self.buoys['Yellow - 1'].loc,
                                            600)
            
            self.locs.append(wayA)
            self.locs.append(wayB)


        #------------ Fleet
        elif self.state == 0:   # initial state -- at start
            self.water.clearWaypoints() # clears waypoints
            # make waypoint on start line
            self.locs = [
                Loc(slat, slon)
                ]
        #------------ Fleet
        elif self.state == 1:  # reaching to windward mark

            lastloc = self.locs[self.currentLocIndex]

            self.water.clearWaypoints() # clears waypoints 
            if self.clear:
                self.locs = []
                self.currentLocIndex = 0      # reset waypoint index
                self.clear = 1 # clear next time unless overridden
            
            # figure out how far away the windward mark is from
            # starting line in cm
            # initial mark is at Loc(slat, slon)

            deltalat_mark = self.buoys['Yellow - 1'].loc.lat - lastloc.lat
            deltalon_mark = self.buoys['Yellow - 1'].loc.lon - lastloc.lon
    
            # final waypoint should be 100cm to the "right" of the
            # windward mark
            radius = 100
            self.angle_to_mark = atan2(deltalat_mark, deltalon_mark)
            self.radius_angle = self.angle_to_mark + pi/2
            mark_radius_lat = cm_to_lat(radius*sin(self.radius_angle))
            mark_radius_lon = cm_to_lon(slat,radius*cos(self.radius_angle))
            self.mark_waypoint = Loc(
                self.buoys['Yellow - 1'].loc.lat+mark_radius_lat,
                self.buoys['Yellow - 1'].loc.lon+mark_radius_lon
                )

            # now do rounding, use radius value above
            # from existing waypoint 180 degrees around to opposite side
            
            numradiuspoints = 2
            r_delta_angle = (160*pi/180) / numradiuspoints

            for iway in range(int(numradiuspoints)):
                theta = fmod(self.radius_angle+r_delta_angle*(iway+1), 2*pi)

                r_dlat = cm_to_lat(radius * sin(theta))
                r_dlon = cm_to_lon(slat,radius * cos(theta))
                
                center = self.buoys['Yellow - 1'].loc

                self.locs.append(Loc(
                        center.lat + r_dlat,
                        center.lon + r_dlon)
                                 )

        ##--------------------  Fleet
        elif self.state == 2:
            # go from current location to second buoy
            # shouldn't need to tack here

            # ADD WAYPOINTS ANYWAYS

            self_loc = self.water.boat.loc
            slat = self_loc.lat
            slon = self_loc.lon

            self.water.clearWaypoints() # clears waypoints 
            if self.clear:
                self.locs = []
                self.currentLocIndex = 0      # reset waypoint index
                self.clear = 1 # clear next time unless overridden
            
            # figure out how far away the windward mark is from
            # starting line in cm
            # initial mark is at Loc(slat, slon)

            deltalat_mark = self.buoys['Yellow - 2'].loc.lat - slat
            deltalon_mark = self.buoys['Yellow - 2'].loc.lon - slon
    
            # final waypoint should be 100cm to the "right" of the
            # windward mark
            radius = 100
            self.angle_to_mark = atan2(deltalat_mark, deltalon_mark)
            self.radius_angle = self.angle_to_mark - pi/2
            mark_radius_lat = cm_to_lat(radius*sin(self.radius_angle))
            mark_radius_lon = cm_to_lon(slat,radius*cos(self.radius_angle))
            self.mark_waypoint = Loc(
                self.buoys['Yellow - 2'].loc.lat+mark_radius_lat,
                self.buoys['Yellow - 2'].loc.lon+mark_radius_lon,
                )
            self.locs.append(self.mark_waypoint)

            # now do rounding, use radius value above
            # from existing waypoint 180 degrees around to opposite side
            
            numradiuspoints = 2
            r_delta_angle = (160*pi/180) / numradiuspoints

            for iway in range(int(numradiuspoints)):
                theta = fmod(self.radius_angle+r_delta_angle*(iway+1), 2*pi)

                r_dlat = cm_to_lat(radius * sin(theta))
                r_dlon = cm_to_lon(slat,radius * cos(theta))
                
                center = self.buoys['Yellow - 2'].loc

                self.locs.append(Loc(
                        center.lat + r_dlat,
                        center.lon + r_dlon)
                                 )

        ##-------------------- Fleet
        elif self.state == 3:
            # go from current location to 3rd buoy
            # shouldn't need to tack here

            self_loc = self.water.boat.loc
            slat = self_loc.lat
            slon = self_loc.lon

            self.water.clearWaypoints() # clears waypoints 
            if self.clear:
                self.locs = []
                self.currentLocIndex = 0      # reset waypoint index
                self.clear = 1 # clear next time unless overridden
            
            # figure out how far away the windward mark is from
            # starting line in cm
            # initial mark is at Loc(slat, slon)

            deltalat_mark = self.buoys['Yellow - 3'].loc.lat - slat
            deltalon_mark = self.buoys['Yellow - 3'].loc.lon - slon
    
            # final waypoint should be 100cm to the "right" of the
            # windward mark
            radius = 100
            self.angle_to_mark = atan2(deltalat_mark, deltalon_mark)
            self.radius_angle = self.angle_to_mark - pi/2
            mark_radius_lat = cm_to_lat(radius*sin(self.radius_angle))
            mark_radius_lon = cm_to_lon(slat,radius*cos(self.radius_angle))
            self.mark_waypoint = Loc(
                self.buoys['Yellow - 3'].loc.lat+mark_radius_lat,
                self.buoys['Yellow - 3'].loc.lon+mark_radius_lon,
                )
            self.locs.append(self.mark_waypoint)

            # now do rounding, use radius value above
            # from existing waypoint 180 degrees around to opposite side
            
            numradiuspoints = 2
            r_delta_angle = (160*pi/180) / numradiuspoints

            for iway in range(int(numradiuspoints)):
                theta = fmod(self.radius_angle+r_delta_angle*(iway+1), 2*pi)

                r_dlat = cm_to_lat(radius * sin(theta))
                r_dlon = cm_to_lon(slat,radius * cos(theta))
                
                center = self.buoys['Yellow - 3'].loc

                self.locs.append(Loc(
                        center.lat + r_dlat,
                        center.lon + r_dlon)
                                 )


        ##--------------------  Fleet

        elif self.state == 4:
            # go to finish line

            prevloc = self.buoys['Yellow - 3'].loc

            self.water.clearWaypoints() # clears waypoints 
            if self.clear:
                self.locs = []
                self.currentLocIndex = 0      # reset waypoint index
                self.clear = 1 # clear next time unless overridden



            # tack to center of start line

            endlat = (self.buoys['Red - Start 1'].loc.lat +
                            self.buoys['Red - Start 2'].loc.lat)/2
            endlon = (self.buoys['Red - Start 1'].loc.lon +
                            self.buoys['Red - Start 2'].loc.lon)/2
            endloc = Loc(endlat, endlon)

            reach_ways = ReachFromTo(prevloc, endloc)

            for way in reach_ways:
                self.locs.append(way)

            self.locs.append(endloc)


            # clearance point
            clear = OffsetFromLineCenter(self.buoys['Red - Start 1'].loc,
                                         self.buoys['Red - Start 2'].loc,
                                         self.buoys['Yellow - 3'].loc,
                                         200)
            self.locs.append(clear)


            
        ##--------------------Fleet
        else:
            self.state = 0
            self.locs = []
            self.timecount = 0
            self.currentLocIndex = 0      # reset waypoint index
            self.updateWaypoints()

        ## Add Waypoints:
        for idx, loc in enumerate(self.locs):
            logger.debug(str(idx) + " " + str(loc.lat))
            self.water.addWaypoint(WaypointItem(loc, idx+1))

    def currentWaypoint(self): # Fleet
        numwaypoints = len(self.locs)

        self.doAnnotations()

        if self.timer_en == True:
            logger.debug("timer is on")
            self.timecount +=1

        logger.debug("waypoint index: " + str(self.currentLocIndex))
        logger.debug("state: " + str(self.state))
        logger.debug("Timer: " + str(5*90 - self.timecount))

        if self.state == -1:
            if 5*1 - self.timecount == 0:
                self.state = 0
                self.currentLocIndex = 0
                self.updateWaypoints()
                self.timer_en = False
                self.timecount = 0
            elif loc_distance_cm(self.water.boat.loc, self.locs[self.currentLocIndex]) < 100:
                logger.debug("Reached waypoint: " + str(self.currentLocIndex))
                self.currentLocIndex +=1
                # if no more waypoints, increment state
                if self.currentLocIndex > numwaypoints - 1:
                    logger.debug("reached end of waypoint list")
                    self.updateWaypoints() # calculate new waypoints 
                    self.currentLocIndex = 0      # reset waypoint index

        elif self.state == 0:
            # if state 0, check if boat crosses the start line (and in the right direction)
            logger.debug("same side as yellow 1: " + str(self.sline_side))
            prev_side = self.sline_side
            self.sline_side = DiffSideLine(self.buoys['Red - Start 1'].loc, 
                                           self.buoys['Red - Start 2'].loc,
                                           self.water.boat.loc,
                                           self.buoys['Yellow - 1'].loc)

            if (prev_side == True) and (self.sline_side == False):
                # just crossed start line
                logger.debug("Crossed start line")

                if BetweenBuoys(self.buoys['Red - Start 1'].loc, 
                                self.buoys['Red - Start 2'].loc,
                                self.water.boat.loc):
                    # passed between buoys, who cares where waypoint was?
                    logger.debug("Between buoys")
                    self.state = 1
                    self.updateWaypoints()

                else:
                    # make this create new waypoints to guide around start marks
                    logger.debug("Missed start line!")

        elif loc_distance_cm(self.water.boat.loc, self.locs[self.currentLocIndex]) < 100:
            logger.debug("Reached waypoint: " + str(self.currentLocIndex))
            if self.state == 1 and self.currentLocIndex == 0:
                self.tacktime = 0
                logger.debug("Reset tack time")
            else:
                self.lasttack = self.tacktime
                self.tacktime = 0 # restart tack time count
                logger.debug("last tack duration: " + str(self.lasttack))
            self.currentLocIndex +=1
            # if no more waypoints, increment state
            if self.currentLocIndex > numwaypoints - 1:
                logger.debug("reached end of waypoint list")
                self.state += 1            #increment state
                self.updateWaypoints() # calculate new waypoints 
                self.currentLocIndex = 0      # reset waypoint index


        return self.locs[self.currentLocIndex]

    def doAnnotations(self): # Fleet
        self.water.clearAnnotations()

        # draw distance line for start buoys
        startLine = DistanceLine(self.water, self.buoys['Red - Start 1'].loc, self.buoys['Red - Start 2'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

        startLine = DistanceLine(self.water, self.buoys['Yellow - 1'].loc, self.buoys['Yellow - 2'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

        startLine = DistanceLine(self.water, self.buoys['Yellow - 2'].loc, self.buoys['Yellow - 3'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)

        startLine = DistanceLine(self.water, self.buoys['Yellow - 3'].loc, self.buoys['Yellow - 1'].loc)
        self.water.annotations.append(startLine)
        self.water.scene.addItem(startLine)
