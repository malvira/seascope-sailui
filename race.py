#!/usr/bin/python

import sys 
import traceback
import logging
import copy
logger = logging.getLogger(__name__)

from navigatorStation import *

from math import *
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from Water import *
from Buoy import *
from Nav import *
from Control import *
from Worldserver import WorldServer
from Obstacle import Obstacle
from Display import (printDisplay, disp)

def leave(type, value, tb):
    traceback.print_exception(type, value, tb)
    sys.exit()

class MainWindow(QtGui.QMainWindow):
    def __init__(self):

        sys.excepthook = leave

        QtGui.QMainWindow.__init__(self)

        self.resize(250, 150)
        self.setWindowTitle('AutoSail')
        self.statusBar().showMessage('Ready')

        self.setupMenu()

        widg = QWidget();
        self.setCentralWidget(widg)

        self.ws = WorldServer()

# flag for water sensor tripping: allows you to change race mode after it has tripped
        self.tripped = 0

        self.water = Water()
        self.water.clicked.connect(self.waterClicked)
        self.water.buoySelected.connect(self.selectBuoy)

        zoom = QSlider(QtCore.Qt.Vertical, widg)
        zoom.valueChanged.connect(self.water.setGrid)
        zoom.setRange(250,100000)
        zoom.setValue(5000)        
        hbox = QHBoxLayout()
        hbox.addWidget(self.water)
        hbox.addWidget(zoom)
        widg.setLayout(hbox)

        vbox = QVBoxLayout()
        hbox.addLayout(vbox)

        self.boatModeBox = QGroupBox()
        self.boatModeLayout = QHBoxLayout()
        self.boatModeBox.setLayout(self.boatModeLayout)
        self.boatModeSimulated = QRadioButton(QString("Simulated"), self.boatModeBox)
        self.boatModeReal = QRadioButton(QString("Real"), self.boatModeBox)
        self.boatModeLayout.addWidget(self.boatModeReal)
        self.boatModeLayout.addWidget(self.boatModeSimulated)
        vbox.addWidget(self.boatModeBox)
        self.boatModeReal.clicked.connect(self.setBoatModeReal)
        self.boatModeSimulated.clicked.connect(self.setBoatModeSimulated)
        self.boatModeReal.setChecked(1)
        self.setBoatModeReal()



        self.PrestartBox = QGroupBox()
        self.PrestartBox.setLayout(QHBoxLayout())
        self.PrestartRaceFoo = QPushButton(QString("Prestart"),self.PrestartBox)
        vbox.addWidget(self.PrestartBox)
        self.PrestartRaceFoo.clicked.connect(self.PrestartRace)


        self.WaypointSetBox = QGroupBox()
        self.WaypointSetBox.setLayout(QHBoxLayout())
        self.WaypointSetReset = QPushButton(QString("Recalculate"),self.WaypointSetBox)
        vbox.addWidget(self.WaypointSetBox)
        self.WaypointSetReset.clicked.connect(self.recalculate)

        self.RestartBox = QGroupBox()
        self.RestartBox.setLayout(QHBoxLayout())
        self.RestartRace = QPushButton(QString("Start Race!"),self.RestartBox)
        vbox.addWidget(self.RestartBox)
        self.RestartRace.clicked.connect(self.restartRace)

        self.raceSelect = QComboBox()
        self.raceSelect.currentIndexChanged.connect(self.changeRace)
        vbox.addWidget(self.raceSelect)
    
        self.buoySelect = QListWidget()
        vbox.addWidget(self.buoySelect)

        self.bibox = QVBoxLayout()
        self.buoyInfoBox = QGroupBox()
        vbox.addWidget(self.buoyInfoBox)
        self.buoyInfoBox.setLayout(self.bibox)
        self.wsBuoySel = QComboBox()
        self.bibox.addWidget(self.wsBuoySel)
        self.wsBuoySel.activated.connect(self.wsBuoySelActivated)

        self.cvbox = QVBoxLayout()
        self.controlBox = QGroupBox()
        vbox.addWidget(self.controlBox)
        self.controlBox.setLayout(self.cvbox)

#initialization
        self.raceBuoys = {
            'Waypoint': 
            {
                'bouys':
                    {
                    'Yellow - 1':self.water.addBuoy(Buoy(type='y', seqno=1, loc=Loc(0,0))),
                    },
                'navigator': navigatorWaypoints
                },
            'RoundingTest': 
            {
                'bouys':
                    {
                    'Yellow - 1':self.water.addBuoy(Buoy(type='y', seqno=1, loc=Loc(0,0))),
                    },
                'navigator': navigatorRounder
                },
            'Fleet Race Near': 
            {
                'bouys':
                    {
                    'Red - Start 1':self.water.addBuoy(Buoy(type='r', seqno=1, loc=Loc(53.8731236594, 10.6994077376))),
                    'Red - Start 2':self.water.addBuoy(Buoy(type='r', seqno=2, loc=Loc(53.872950326, 10.6993545248))),
                    'Yellow - 3':self.water.addBuoy(Buoy(type='y', seqno=3, loc=Loc(53.8728096375, 10.6996361914))),
                    'Yellow - 2':self.water.addBuoy(Buoy(type='y', seqno=2, loc=Loc(53.8726773552, 10.69893))),
                    'Yellow - 1':self.water.addBuoy(Buoy(type='y', seqno=1, loc=Loc(53.8731652896, 10.6983383333))),
                    },
                'navigator': navigatorFleetRace
                },            
            'Fleet Race Far': 
            {
                'bouys':
                    {
                    'Red - Start 1':self.water.addBuoy(Buoy(type='r', seqno=1, loc=Loc(53.8731236594, 10.6994077376))),
                    'Red - Start 2':self.water.addBuoy(Buoy(type='r', seqno=2, loc=Loc(53.872950326, 10.6993545248))),
                    'Yellow - 1':self.water.addBuoy(Buoy(type='y', seqno=1, loc=Loc(53.8728096375, 10.6996361914))),
                    'Yellow - 2':self.water.addBuoy(Buoy(type='y', seqno=2, loc=Loc(53.8734572, 10.6995376))),
                    'Yellow - 3':self.water.addBuoy(Buoy(type='y', seqno=3, loc=Loc(53.8731652896, 10.6983383333))),
                    },
                'navigator': navigatorFleetRace
                },            
            'Collision Avoidance': 
            {
                'bouys':
                    {
                    'Red - Start 1':self.water.addBuoy(Buoy(type='r', seqno=1, loc=Loc(53.87279459,10.69934227))),
                    'Red - Start 2':self.water.addBuoy(Buoy(type='r', seqno=2, loc=Loc(53.87276125,10.69915862))),
                    'Yellow - 1':self.water.addBuoy(Buoy(type='y', seqno=1, loc=Loc(53.87314201,10.69893122))),
                    'White - Obstacle':self.water.addBuoy(Buoy(type='w', loc=Loc(0,0))),
                    },
                'navigator': navigatorObstacle
                },
            'Station Holding': 
            {
                'bouys':
                    {
                    'Yellow - Entrance 1':self.water.addBuoy(Buoy(type='y', seqno=1, loc=Loc(53.87293532,10.69877022))),
                    'Yellow - Entrance 2':self.water.addBuoy(Buoy(type='y', seqno=2, loc=Loc(53.8730609,10.6991446))),
                    'Red - Exit 1':self.water.addBuoy(Buoy(type='r', seqno=1, loc=Loc(53.87271962,10.698863419))),
                    'Red - Exit 2':self.water.addBuoy(Buoy(type='r', seqno=2, loc=Loc(53.87286362,10.69925298))),
                    },
                'navigator': navigatorStationSimple
                },
            'Navigation': 
            {
                'bouys':
                    {
                    'Red - Start 1':self.water.addBuoy(Buoy(type='r', seqno=1, loc=Loc(53.8733209,10.69894212))),
                    'Red - Start 2':self.water.addBuoy(Buoy(type='r', seqno=2, loc=Loc(53.8732796,10.69898218))),
                    'Red - Upwind Mark':self.water.addBuoy(Buoy(type='r', seqno=3, loc=Loc(53.87310545,10.69865096))),
                    'Yellow - Exit 1':self.water.addBuoy(Buoy(type='y', seqno=1, loc=Loc(53.87337356,10.6988904))),
                    'Yellow - Exit 2':self.water.addBuoy(Buoy(type='y', seqno=2, loc=Loc(53.87325592,10.69903128))),
                    },
                'navigator': navigatorNavigation
                },
            'Endurance': 
            {
                'bouys':
                    {
                    'Red - Start 1':self.water.addBuoy(Buoy(type='r', seqno=1, loc=Loc(0,0))),
                    'Red - Start 2':self.water.addBuoy(Buoy(type='r', seqno=2, loc=Loc(0,0))),
                    'Yellow - 1':self.water.addBuoy(Buoy(type='y', seqno=1, loc=Loc(0,0))),
                    'Yellow - 2':self.water.addBuoy(Buoy(type='y', seqno=2, loc=Loc(0,0))),
                    'Yellow - 3':self.water.addBuoy(Buoy(type='y', seqno=3, loc=Loc(0,0))),
                    },
                'navigator': navigatorFleetRace
                },
            }

        self.setupRaceSelect()
        self.changeRace(self) #dummy change to do initial setup

        self.manual.setChecked(True)
        self.setControlManual()

        self.setCenterLubeckTent()

    def recalculate(self):
        logging.debug("recalculate")
        self.water.clearWaypoints()
        self.navigator.clear = 1
        self.navigator.currentLocIndex = 0
        self.navigator.updateWaypoints()

    def restartRace(self):
        logging.debug("start race timer")
        self.water.clearWaypoints()
        self.navigator.clear = 1
        self.navigator.state = -1
        self.navigator.timer_en = True
        self.navigator.timeref = time.time()
        self.navigator.currentLocIndex = 0
        self.navigator.updateWaypoints()

    def startRace(self):
        self.water.clearWaypoints()
        self.navigator.clear = 1
        self.navigator.state = 0
        self.navigator.currentLocIndex = 0
        self.navigator.updateWaypoints()


    def PrestartRace(self):
        logging.debug("prestart mode")
        self.water.clearWaypoints()
        self.navigator.clear = 1
        self.navigator.state = -1
        self.navigator.currentLocIndex = 0
        self.navigator.updateWaypoints()

    def setBoatModeReal(self):
        logging.info("boat mode real")
        self.water.boat.mode = 'real'

    def setBoatModeSimulated(self):
        logging.info("boat mode simulated")
        self.water.boat.mode = 'simulated'

    def selectBuoy(self, buoy):
        buoys = self.buoySelect.findItems(QString(buoy.txt), Qt.MatchExactly)
        self.buoySelect.setCurrentItem(buoys[0])

    def setupRaceSelect(self):
        for race in self.raceBuoys.keys():
            self.raceSelect.addItem(race)

    def changeRace(self, race):
        racetxt = str(self.raceSelect.currentText())
        logging.info("race changed to " + racetxt)
        self.buoySelect.clear()
        for race in self.raceBuoys.keys():
            for buoy in self.raceBuoys[race]['bouys']:
                self.raceBuoys[race]['bouys'][buoy].item.hide()
        for buoytxt in self.raceBuoys[racetxt]['bouys'].keys():
            self.buoySelect.addItem(buoytxt)
            self.raceBuoys[racetxt]['bouys'][buoytxt].item.show()
            self.raceBuoys[racetxt]['bouys'][buoytxt].txt = buoytxt
        self.navigator = self.raceBuoys[racetxt]['navigator'](self.water, self.raceBuoys[racetxt]['bouys'])

    def waterClicked(self, loc):
        logging.info("water clicked " + str(loc.lat) + ' ' + str(loc.lon))
        try:
            txt = str(self.buoySelect.currentItem().text())
            logging.info("move buoy " + txt)
            buoy = self.raceBuoys[str(self.raceSelect.currentText())]['bouys'][txt]
            buoy.setLoc(loc)
#            self.navigator.updateWaypoints()
        except AttributeError:
            pass

    def setupMenu(self):
        exit = QtGui.QAction(QtGui.QIcon('/usr/share/icons/gnome/16x16/actions/exit.png'), 'Exit', self)
        exit.setShortcut('Ctrl+C')
        exit.setStatusTip('Exit application')
        self.connect(exit, QtCore.SIGNAL('triggered()'), QtCore.SLOT('close()'))

        self.statusBar()

        menubar = self.menuBar()

        file = menubar.addMenu('&File')
        file.addAction(exit)

#Map center select
        self.centers = menubar.addMenu('&Center')

        boat = QAction('Boat', self)
        self.addCenterAction(boat)
        boat.triggered.connect(self.setCenterBoat)

        charles = QAction('Charles', self)
        self.addCenterAction(charles)
        charles.triggered.connect(self.setCenterCharles)

        walden = QAction('Walden', self)
        walden.triggered.connect(self.setCenterWalden)
        self.addCenterAction(walden)

        lubecktent = QtGui.QAction('Lubeck Tent', self)
        lubecktent.triggered.connect(self.setCenterLubeckTent)
        self.addCenterAction(lubecktent)

        lubeckshore = QtGui.QAction('Lubeck Shore', self)
        lubeckshore.triggered.connect(self.setCenterLubeckShore)
        self.addCenterAction(lubeckshore)

        mystic = QtGui.QAction('Mystic', self)
        mystic.triggered.connect(self.setCenterMystic)
        self.addCenterAction(mystic)

        candlewood = QtGui.QAction('Candlewood', self)
        candlewood.triggered.connect(self.setCenterCandlewood)
        self.addCenterAction(candlewood)

        spy = QtGui.QAction('Spy Pond', self)
        spy.triggered.connect(self.setCenterSpy)
        self.addCenterAction(spy)

        redwire = QtGui.QAction('Redwire', self)
        redwire.triggered.connect(self.setCenterRedwire)
        self.addCenterAction(redwire)

#Boat control scheme selection
        self.controls = menubar.addMenu('&Controls')
        self.controlGroup = QActionGroup(self)

        self.manual = QAction('Manual Control', self)
        self.addControlAction(self.manual)
        self.manual.triggered.connect(self.setControlManual)

        self.rudcompwind = QAction('Rudder<->Compass, GPS, Wind', self)
        self.addControlAction(self.rudcompwind)
        self.rudcompwind.triggered.connect(self.setControlRudCompassWind)

        self.rudcompass = QAction('Rudder<->Compass, GPS', self)
        self.addControlAction(self.rudcompass)
        self.rudcompass.triggered.connect(self.setControlRudCompass)

        self.rudgpswind = QAction('GPS + Rudder + Wind', self)
        self.addControlAction(self.rudgpswind)
        self.rudgpswind.triggered.connect(self.setControlRudGPSWind)

        self.rudgps = QAction('GPS + Rudder', self)
        self.addControlAction(self.rudgps)
        self.rudgps.triggered.connect(self.setControlRudGPS)

    def setControlManual(self):
        self.clearControl()
        self.man_rud = QSlider(QtCore.Qt.Horizontal, self.controlBox)
        self.cvbox.addWidget(self.man_rud)
        self.man_rud.setRange(-90,90)
        self.man_rud.valueChanged.connect(self.water.boat.setRudder)
        self.control = controlManual(self.water.boat)

    def setControlRudCompassWind(self):
        self.clearControl()
        self.control = controlRudderCompassWind(self.water.boat)

    def setControlRudCompass(self):
        self.clearControl()
        self.control = controlRudderCompass(self.water.boat)

        self.windDir = QSlider(QtCore.Qt.Horizontal, self.controlBox)
        self.cvbox.addWidget(self.windDir)
        self.windDir.setRange(0,359)
        self.windDir.valueChanged.connect(self.water.setWindDir)

    def setControlRudGPSWind(self):
        self.clearControl()
        self.control = controlRudderGPSWind(self.water.boat)

    def setControlRudGPS(self):
        self.clearControl()
        self.control = controlRudderGPS(self.water.boat)

        self.windDir = QSlider(QtCore.Qt.Horizontal, self.controlBox)
        self.cvbox.addWidget(self.windDir)
        self.windDir.setRange(0,359)
        self.windDir.valueChanged.connect(self.water.setWindDir)

    def clearControl(self):
        while self.cvbox.count() > 0:
            item = self.cvbox.takeAt(0)
            if not item:
                continue

            w = item.widget()
            if w:
                w.deleteLater()

    def addControlAction(self, act):
        act.setCheckable(True)
        self.controls.addAction(act)
        self.controlGroup.addAction(act)

    def addCenterAction(self, act):
        self.centers.addAction(act)

    def setCenterBoat(self):
        logging.info("set center boat")
        self.water.setCenter(copy.copy(self.water.boat.loc))
        
    def setCenterCharles(self):
        logging.info("set center charles")
        self.water.setCenter(Loc(42.35389793852932, -71.08295917510986))

    def setCenterWalden(self):
        logging.info("set center walden")
        self.water.setCenter(Loc(42.493903146838186, -70.99459648132324))

    def setCenterLubeckTent(self):
        logging.info("set center lubeck tent")
        self.water.setCenter(Loc(53.87247, 10.69876))

    def setCenterLubeckShore(self):
        logging.info("set center lubeck shore")
        self.water.setCenter(Loc(53.87247, 10.69910))

    def setCenterMystic(self):
        logging.info("set center upper mystic")
        self.water.setCenter(Loc(42.43520840525484, -71.14969253540039))

    def setCenterCandlewood(self):
        logging.info("set center candlewood")
        self.water.setCenter(Loc(41.54759644650707, -73.44396829605103))
        
    def setCenterSpy(self):
        logging.info("set center spy")
        self.water.setCenter(Loc(42.40815358973853, -71.15479946136475))

    def setCenterRedwire(self):
        logging.info("set center redwire")
        self.water.setCenter(Loc(42.396642250316845, -71.06411933898926))

    def update(self):
        self.control.updateBoat(win.water.boat, self.navigator.currentWaypoint(), win.water)
        win.water.setBoatLoc(win.water.boat.loc)
#        self.ws.sendBoatItem(win.water.boat)
#        self.recordUpdate()
        self.buoyUpdate()
        self.obstacleUpdate()
        printDisplay(disp)
        if win.water.boat.waterlevel > 2000 and self.tripped == 0:
            self.tripped = 1
#            self.raceSelect.setCurrentIndex(self.raceSelect.findText('Waypoint'))

    def obstacleUpdate(self):
        self.water.clearObstacles()
        try:
            for rec in self.ws.records:
                if (rec['TYPE'] == 'CircularObstacle'):
                    self.water.addObstacle(Obstacle(self.water, Loc(float(rec['Lat']), float(rec['Lon'])), rec['ID'], float(rec['Radius'])))
        except TypeError:
            pass

    def buoyUpdate(self):
        for buoytxt in self.raceBuoys[str(self.raceSelect.currentText())]['bouys'].keys():
            buoy = self.raceBuoys[str(self.raceSelect.currentText())]['bouys'][buoytxt]
            if buoy.wsid != '':
                try:
                    for rec in self.ws.records:
                        if (rec['TYPE'] == 'Buoy' or rec['TYPE'] == 'CircularObstacle') and rec['ID'] == buoy.wsid:
                            buoy.loc.lat = float(rec['Lat'])
                            buoy.loc.lon = float(rec['Lon'])
                except TypeError:
                    pass

    def recordUpdate(self):
        self.ws.records = self.ws.getRecords()
        self.wsBuoySel.clear()
        self.wsBuoySel.addItem('<None>')
        logger.debug(type(self.buoySelect.currentItem()))
        if isinstance(self.buoySelect.currentItem(), QListWidgetItem):
            try: 
                for rec in self.ws.records:
                    if rec['TYPE'] == 'Buoy' or rec['TYPE'] == 'CircularObstacle':
                        self.wsBuoySel.addItem(rec['ID'])
                        logger.debug(self.buoySelect.currentItem().text())
                        buoy = self.raceBuoys[str(self.raceSelect.currentText())]['bouys'][str(self.buoySelect.currentItem().text())]
                        if buoy.wsid != '':
                            self.wsBuoySel.setCurrentIndex(self.wsBuoySel.findText(buoy.wsid))
            except TypeError:
                pass
                
    def wsBuoySelActivated(self, txt):
        logger.debug(self.wsBuoySel.currentText())
        try:
            buoy = self.raceBuoys[str(self.raceSelect.currentText())]['bouys'][str(self.buoySelect.currentItem().text())]
            buoy.wsid = self.wsBuoySel.currentText()
        except AttributeError:
            pass

if __name__ == '__main__':
    app = QApplication(sys.argv)

    logging.basicConfig(level=logging.DEBUG)
    waterlog = logging.getLogger('Water')
    waterlog.setLevel(logging.WARN)
    buoylog = logging.getLogger('Buoy')
    buoylog.setLevel(logging.WARN)
    boatlog = logging.getLogger('boat')
    boatlog.setLevel(logging.WARN)
    controllog = logging.getLogger('Control')
    controllog.setLevel(logging.INFO)
    navlog = logging.getLogger('Nav')
    navlog.setLevel(logging.INFO)
    stationlog = logging.getLogger('navigatorStation')
    stationlog.setLevel(logging.DEBUG)
    fleetlog = logging.getLogger('navigatorFleetRace')
    fleetlog.setLevel(logging.INFO)
    obstaclelog = logging.getLogger('navigatorObstacle')
    obstaclelog.setLevel(logging.INFO)
    wslog = logging.getLogger('Worldserver')
    wslog.setLevel(logging.CRITICAL)

    win = MainWindow()
    win.show()

    updateTimer = QTimer()
    updateTimer.start(200)
    updateTimer.timeout.connect(win.update)

    heartbeat = QTimer()
    heartbeat.start(1000)
    heartbeat.timeout.connect(win.water.boat.toggleLED)    


    sys.exit(app.exec_())
